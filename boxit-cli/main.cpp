#include "global.h"
#include <services/user_management.h>
#include <services/package_management.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <optional>
#include <functional>
#include <fstream>
#include <boost/process.hpp>
#include <utils.h>
#include <unistd.h>
#include <fmt/printf.h>
#include <fmt/color.h>
#include <grpc++/grpc++.h>

#include <utils/cli_io.h>

static std::shared_ptr<grpc::Channel> channel;


inline std::string load_cert(const std::string& key_cert_file){
    auto path = global::settings.get<std::string>("certs-path").value_or("certs") + "/" + key_cert_file;
        if(!std::filesystem::exists(path)){
            return "";
        }

    std::ifstream cert_file(path);

    return {std::istreambuf_iterator<char>(cert_file), std::istreambuf_iterator<char>()};

}

inline bool check_parameter_number(const std::vector<std::string>& parameters, uint64_t size)
{
    if(parameters.size() < size){
        cli_io::err("Not enough parameters");
        return false;
    }
    if(parameters.size() > size){
        cli_io::err("Too much parameters");
        return false;
    }
    return true;
}
using command_func = std::function< int(const std::vector<std::string>& params) >;

std::unordered_map< std::string, command_func > commands {
    {"useradd", [](const std::vector<std::string>& parameters){
            if(!check_parameter_number(parameters, 3)){
                return 1;
            }
            if(!user_management(channel).add_user(parameters[1], utils::sha256(parameters[2]))){
                cli_io::err("User add failed");
                return 1;
            }

            cli_io::info("Successfully added new user");
            return 0;
        }},
    {"userdel", [](const std::vector<std::string>& parameters){
            if(!check_parameter_number(parameters, 2)){
                return 1;
            }
            if(!user_management(channel).remove_user(parameters[1])){
                cli_io::err("User remove failed");
                return 1;
            }
            cli_io::info(fmt::format("Successfully removed user {}", parameters[1]));
            return 0;
        }},
    {"sync", [](const std::vector<std::string>& parameters){

            if(!package_management(channel).sync(parameters.size()>1?parameters[1]:"")){
                    return 1;
            }
            cli_io::info("Successfully started synchronization");
            return 0;
        }},
    {"snap", [](const std::vector<std::string>& parameters){
            if(!check_parameter_number(parameters, 3)){
                return 1;
            }
            if(!package_management(channel).snap(parameters[1], parameters[2])){
                return 1;
            }
            cli_io::info(fmt::format("Successfully started snapshot from {} to {}",parameters[1], parameters[2]));
            return 0;
        }},
    {"mount", [](const std::vector<std::string>& parameters){
            if(!check_parameter_number(parameters, 2)){
                return 1;
            }
            namespace bp = boost::process;
            auto mount = bp::child("./boxit-fuse-app", parameters[1], bp::std_out > bp::null );
            mount.join();
            if( mount.exit_code() != 0 ){
                cli_io::err("Failed to mount");
                return 1;
            }
            cli_io::info("Successfully mounted");
            return 0;
        }},
    {"compare", [](const std::vector<std::string>& parameters){
            if(!check_parameter_number(parameters, 3)){
                return 1;
            }

            auto comparison = package_management(channel).compare(parameters[1], parameters[2]);
            if(!comparison){
                return 1;
            }
            if(comparison->empty()){
                cli_io::info("Repositories are equal");
                return 0;
            }
            std::cout <<fmt::format("┌{0:─^30}┬{0:─^30}┬{0:─^30}┐\n","");
            std::cout <<fmt::format("│{:^30}│{:^30}│{:^30}│\n", "Package",parameters[1], parameters[2]);
            std::cout <<fmt::format("├{0:─^30}┼{0:─^30}┼{0:─^30}┤\n","");

            
            for(const auto& [name, evrs] : *comparison){
                auto first_evr = evrs.first;
                auto second_evr = evrs.second;
                if(first_evr.string()!=second_evr.string()){
                    std::cout << fmt::format("│{:^30}│{:^30}│{:^30}│\n", name, first_evr.string(), second_evr.string());
                }
            }

            std::cout <<fmt::format("└{0:─^30}┴{0:─^30}┴{0:─^30}┘\n","");
            return 0;
        }}

};

std::string prompt_to_config(const std::string& key, const std::string& description, bool allow_empty = false){
    auto value = global::settings.get<std::string>(key);
    if(!value){
        
        cli_io::question(description, allow_empty);

        global::settings.set(key, std::string(*value));

        global::settings.save();
        
    }
    return *value;
}

void passwd(){
    global::credentials_manager.reset(global::current_profile);
    global::credentials_manager.profile_prompt(global::current_profile);
}

void boxit_grpc_logger(gpr_log_func_args* args) {
    switch(args->severity){
    case GPR_LOG_SEVERITY_ERROR:
        cli_io::err(args->message);
        break;
    case GPR_LOG_SEVERITY_DEBUG:
    case GPR_LOG_SEVERITY_INFO:
        cli_io::info(args->message);
    }

}

bool setup_grpc_channel(){
    std::string cert = load_cert("server.crt");

    grpc::SslCredentialsOptions ssl_opts = {.pem_root_certs = cert};

    auto ssl_creds = grpc::SslCredentials(ssl_opts);
    grpc::ChannelArguments chargs;
    auto target_override = global::settings.get<std::string>("ssl-target");
    if(target_override.has_value()){
        chargs.SetSslTargetNameOverride(target_override.value());
    }
    auto creds = global::credentials_manager.profile_prompt();

    channel = grpc::CreateCustomChannel(creds.host,
                          ssl_creds, chargs);

    if(channel->GetState(true)==grpc_connectivity_state::GRPC_CHANNEL_SHUTDOWN
        || channel->GetState(true)==grpc_connectivity_state::GRPC_CHANNEL_TRANSIENT_FAILURE){
        cli_io::err(fmt::format("Server {} connection failed", creds.host));
        return false;
    }
    return true;
}
        

int main(int argc, char** argv){
//     namespace bp = boost::process;
//     simppl::dbus::Dispatcher disp("bus:session");
//     bp::spawn("./boxit-keyring-app", bp::std_out > bp::null );
// 
//     DBusClient cl(disp);
//     bool available = false;
//     std::optional<credentials> crd;
//     while(!available){
//         try {
// 
//             crd = cl.credentials();
//             
//         } catch (...) {
//             using namespace std::chrono_literals;
//             std::this_thread::sleep_for(100ms);
// 
//             continue;
//         }
//         available = true;
//     }
//     
//     if(!crd){
//         cl.set_credentials({"127.0.0.1", "root", "hlaaly"});
//     } else {
//         std::cout<<crd->host;
//     }
//     return 0;
    auto factory = std::make_unique<storage_secret_factory>();
    global::credentials_manager.set_backend(factory.get());
    gpr_set_log_function(boxit_grpc_logger);
    
    std::vector<std::string> args(argv+1, argv+argc);

    if(args.empty() || args[0]=="help"){
        fmt::print(fmt::emphasis::bold, utils::box);

        
        std::cout<<fmt::format(
            "Usage: {} OPERATION\n\n"
            "OPERATION:\n\n"
            "   sync [repo] - synchronize the database [for a specific repository]\n"
            "   compare [source] [target] - compare source branch and target branch\n"
            "   mount [folder] - mounts boxitfs as a local filesystem\n"
            "   snap [source] [target] - snapshot source branch to the target branch\n"
            "   useradd [name] - add a new user\n"
            "   userdel [name] - delete an existing user\n\n",
            argv[0]
        );
        return args.empty()?1:0;
    }
    if(args[0] == "passwd"){
        passwd();
        return 0;
    }
    if(!commands.contains(args[0])){
        cli_io::err("Invalid operation. Use help for the list of operations");
        return 1;
    }
    if(!setup_grpc_channel()){
        return 1;
    }
    
    return commands.at(argv[1])(args);
}
