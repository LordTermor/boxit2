#include "user_management.h"
#include <protos/user.pb.h>
user_management::user_management(std::shared_ptr<grpc::ChannelInterface> channel):service_base(channel)
{

}

bool user_management::add_user(const std::string &name, const std::string &password)
{
    AddUserRequest rq;
    Empty rp;

    rq.set_name(name);
    rq.set_password(password);

    return m_stub->AddUser(create_default_context().get(), rq, &rp).ok();
}

bool user_management::remove_user(const std::string &name)
{
    RemoveUserRequest request;
    Empty rp;
    request.set_name(name);

    return m_stub->RemoveUser(create_default_context().get(), request, &rp).ok();

}
