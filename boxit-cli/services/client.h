#pragma once
#include <iostream>
#include <boost/asio.hpp>

class client
{
public:
    client();

    static int send(boost::asio::streambuf& ibuffer, boost::asio::streambuf& obuffer);

};
