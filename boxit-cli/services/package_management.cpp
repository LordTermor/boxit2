#include "package_management.h"
#include <protos/package.pb.h>
#include <routes.h>
#include <utils/cli_io.h>

bool package_management::sync(const std::string &repo)
{
    SyncRequest rq;

    rq.set_repo(repo);

    SyncReply rep;
    
    auto status = m_stub->Sync(create_default_context().get(),rq,&rep);

    if(!status.ok()){
        cli_io::err(status.error_message());
    }
    
    return status.ok();

}

bool package_management::snap(const std::string &source_branch, const std::string &target_branch)
{
    SnapshotRequest rq;

    rq.set_source_branch(source_branch);
    rq.set_target_branch(target_branch);

    SnapshotReply repl;
    
    grpc::ClientContext ctx;

    auto status = m_stub->Snap(create_default_context().get(), rq, &repl);

    if(!status.ok()){
        cli_io::err(status.error_message());
        return false;
    }

    return status.ok();
}

std::optional<branch_comparison_t> package_management::compare(const branch_t &source_branch, const branch_t &target_branch) const
{
    CompareRequest rq;

    rq.set_source_branch(source_branch);
    rq.set_target_branch(target_branch);

    CompareReply repl;
    grpc::ClientContext ctx;

    auto status = m_stub->Compare(create_default_context().get(),rq,&repl);


    branch_comparison_t result;

    if(!status.ok()){
        cli_io::err(status.error_message());
        return {};
    }

    for(const auto& el : repl.comparison() ){
        evr first {
            .epoch = el.second.first().epoch(),
                    .version = el.second.first().version(),
                    .release = el.second.first().revision()

        };
        evr second  {
            .epoch = el.second.second().epoch(),
                    .version = el.second.second().version(),
                    .release = el.second.second().revision()
        };
        result[el.first] = {first,second};
    }

    return result;
}
