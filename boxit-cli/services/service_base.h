#pragma once
#include <memory>
#include <grpc++/grpc++.h>
#include <global.h>
#include <utils/secret.h>
#include <fmt/format.h>

template <typename TService>
class service_base
{
public:
    service_base(std::shared_ptr<grpc::ChannelInterface> channel)
        : m_stub( new typename TService::Stub(channel)){
    }

protected:
    std::shared_ptr<grpc::ClientContext> create_default_context() const{
        std::shared_ptr<grpc::ClientContext> ctx(new grpc::ClientContext);
       
        auto creds = global::credentials_manager.get(global::current_profile);
        
        ctx->AddMetadata("username", creds->username);
        ctx->AddMetadata("password", creds->password.value_or(""));
                

        return ctx;
    }
    std::unique_ptr<typename TService::Stub> m_stub;

};
