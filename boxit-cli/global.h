#pragma once
#include <settings_manager.h>
#include <string>
#include <utils/credentials_manager.h>
namespace global {
    inline settings_manager settings("config.yml");
    const int default_port = 5000;
    inline credentials_manager_cli credentials_manager(settings);
    inline std::string current_profile = "default";
}
