#include "node_root.h"
#include <fs_accessor.h>


fuse_result<dirs_t> node_root::readdir()
{
    return fs_accessor(m_channel).get_root();
}
int node_root::access(int type)
{
    return 0;
}

fuse_result<struct stat> node_root::getattr()
{
    auto st = node_base_folder::getattr();
    if(st)
    {
        st->st_nlink = 2;
    }


    return st;
}
