#include "node_repo.h"
#include <fs_accessor.h>



fuse_result<dirs_t> node_repo::readdir()
{
    return fs_accessor(m_channel).get_repo_packages(m_branch,m_repo);
}

fuse_result<struct stat> node_repo::getattr()
{
    auto stats = node_base_folder::getattr();
    
    if(!stats){
        return {};
    }
    stats->st_mode += S_IWUSR + S_IWGRP;
    
    return stats;
}

int node_repo::access(int type)
{
    if(type == F_OK){
        return fs_accessor(m_channel).exists(m_branch, m_repo);
    }
    return 0;
}
