#include "node_package.h"
#include <fs_accessor.h>
fuse_result<struct stat> node_package::getattr()
{

    return node_base_file::getattr();
}

int node_package::write(std::istream& data, off_t offset)
{
    auto written_now = fs_accessor(m_channel).write_overlay(m_pkg.filename(), offset, data);
    bytes_written += written_now;

    return written_now;
}

int node_package::unlink()
{
    return fs_accessor(m_channel).remove(m_branch, m_repo, m_pkg)?0:-1;
}

int node_package::release()
{
    if(bytes_written!=0){
        return fs_accessor(m_channel).register_package(m_branch, m_repo, m_pkg)?0:-1;
    }
    return 0;
}

int node_package::access(int mode)
{
    if(mode == F_OK){
        if(!fs_accessor(m_channel).exists(m_branch, m_repo, m_pkg.filename())){
            return -ENOENT;
        }
    }
    return 0;
}
