#include "node_signature.h"
#include <fs_accessor.h>
int node_signature::write(std::istream& data, off_t offset)
{
    auto written_now = fs_accessor(m_channel).write_overlay(m_name, offset, data);
    bytes_written += written_now;

    return written_now;
}

int node_signature::unlink()
{
    return fs_accessor(m_channel).remove(m_branch,m_repo, package(m_name.substr(0, m_name.length()-4)));
}

int node_signature::release()
{
    if(bytes_written!=0){
        return fs_accessor(m_channel).register_package(m_branch,m_repo, package(m_name.substr(0, m_name.length()-4)));
    }
    return 0;
}

int node_signature::access(int type)
{
    if(type == F_OK)
    {
        return fs_accessor(m_channel).exists(m_branch, m_repo, m_name)?0:-ENOENT;
    }
    return 0;
}
