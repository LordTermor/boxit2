#include "node_branch.h"
#include <fs_accessor.h>


fuse_result<dirs_t> node_branch::readdir()
{
     return fs_accessor(m_channel).get_branch(m_name);
}
int node_branch::access(int type)
{
    if(type == F_OK){
        return fs_accessor(m_channel).exists(m_name)?0:-ENOENT;
    }
    return 0;
}
