#pragma once
#include "node_base.h"
#include <types.h>

class node_repo : public node_base_folder
{
public:
    explicit node_repo(const branch_t& branch_name, const repo_t& repo_name):m_branch(branch_name),m_repo(repo_name){}
    fuse_result<struct stat> getattr() override;
    fuse_result<dirs_t> readdir() override;
    int access(int type) override;
private:
    branch_t m_branch;
    repo_t m_repo;
};
