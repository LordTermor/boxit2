#include "boxitfs_fuse_operations.h"
#include "boxitfs.h"
namespace boxitfs_operations {

    static auto self(){
        return reinterpret_cast<boxitfs*>(fuse_get_context()->private_data);
    }

    int readdir(const char *path, void * buffer, fuse_fill_dir_t filler, off_t offset, [[maybe_unused]] fuse_file_info *fi, [[maybe_unused]] fuse_readdir_flags flags)
    {
        return self()->readdir(path, buffer, filler, offset,*fi,flags);
    }

    int getattr(const char * path,struct stat * stat, [[maybe_unused]] fuse_file_info *fi)
    {
        return self()->getattr(path, *stat, *fi);
    }

    int open(const char* path, fuse_file_info *fi)
    {
        return self()->open(path,*fi);
    }
    int opendir(const char* path, struct fuse_file_info *fi)
    {
        return 0;
    }
    int create(const char *path, mode_t mode, struct fuse_file_info *fi)
    {


    }

    int write(const char* path, const char* buffer, size_t size, off_t offset,struct fuse_file_info *fi)
    {
        return self()->write(path, buffer, size, offset,*fi);
    }
    int unlink (const char *path)
    {
        return self()->unlink(path);
    }

    int release(const char *path, struct fuse_file_info *fi)
    {
        return self()->release(path, *fi);
    }

    int statfs(const char* path, struct statvfs* sfs)
    {
        return self()->statfs(path,*sfs);
    }
    int access(const char* path, int type)
    {
        return self()->access(path,type);
    }
}

boxitfs_fuse_operations::boxitfs_fuse_operations()
{
    fuse_operations::readdir = &boxitfs_operations::readdir;
    fuse_operations::getattr = &boxitfs_operations::getattr;
    fuse_operations::write = &boxitfs_operations::write;
    fuse_operations::unlink = &boxitfs_operations::unlink;
    fuse_operations::release = &boxitfs_operations::release;
    fuse_operations::open = &boxitfs_operations::open;
    fuse_operations::create = &boxitfs_operations::create;
    fuse_operations::statfs = &boxitfs_operations::statfs;
    fuse_operations::opendir =&boxitfs_operations::opendir;
    fuse_operations::access = &boxitfs_operations::access;
}
