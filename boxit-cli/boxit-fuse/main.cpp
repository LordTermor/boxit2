#include <fuse3/fuse.h>

#include <unistd.h>
#include <sys/types.h>
#include "boxitfs_fuse_operations.h"
#include "boxitfs.h"
#include "fs_accessor.h"
#include <fstream>

static boxitfs_fuse_operations operations;


inline std::string load_cert(const std::string& key_cert_file){

    auto path = global::settings.get<std::string>("certs-path").value_or("certs") + "/" + key_cert_file;
    std::ifstream cert_file(path);

    return {std::istreambuf_iterator<char>(cert_file), std::istreambuf_iterator<char>()};

}


int main( int argc, char *argv[] )
{
    auto factory = std::make_unique<storage_secret_factory>();
    global::credentials_manager.set_backend(factory.get());
    
    auto host = *global::settings.get<std::string>({"profiles", "default", "host"});

    std::string cert = load_cert("server.crt");

    grpc::SslCredentialsOptions ssl_opts = {.pem_root_certs = cert};

    auto bfs = std::make_shared<boxitfs>();
    
    grpc::ChannelArguments chargs;
    auto target_override = global::settings.get<std::string>("ssl-target");
    if(target_override.has_value()){
        chargs.SetSslTargetNameOverride(target_override.value());
    }

    bfs->set_сhannel(grpc::CreateCustomChannel(host, grpc::SslCredentials(ssl_opts), chargs));
    return fuse_main( argc, argv, &operations, bfs.get());
}
