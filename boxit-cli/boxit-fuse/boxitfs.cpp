#include "boxitfs.h"
#include <iostream>

#include <nodes/node_root.h>
#include <nodes/node_branch.h>
#include <nodes/node_repo.h>
#include <nodes/node_package.h>
#include <nodes/node_signature.h>

std::shared_ptr<node_base> boxitfs::get_node_fi(const fuse_file_info& fi){
    if(fi.fh!=0){
        return m_handles[fi.fh];
    }
    return nullptr;
}
bool boxitfs::set_node_fi(fuse_file_info& fi, const std::shared_ptr<node_base>& node){
    if(node!=nullptr && fi.fh==0){
        fi.fh = reinterpret_cast<uint64_t>(node.get());
        m_handles[fi.fh] = node;
        return true;
    }
    return false;
}

int boxitfs::getattr(const std::filesystem::path &path, struct stat &st, fuse_file_info &fi)
{
    auto node = get_node(path);

    if(!node){
        return -ENOENT;
    }

    auto st_opt = node->getattr();
    if(st_opt){
        st = *st_opt;
        return 0;
    }
    return st_opt.error();


}

int boxitfs::open(const std::filesystem::path& path, fuse_file_info& fi)
{
    auto node = get_node(path);

    if(!node){
        return -ENOENT;
    }

    set_node_fi(fi, node);
    return 0;
}

int boxitfs::create(const std::filesystem::path& path, mode_t mode, fuse_file_info& fi)
{
    auto node = get_node(path);

    std::stringstream str;

    auto result = node->write(str, 0);
    auto result1 = node->release();

    set_node_fi(fi, node);

    return (result!=0 || result1!=0)?-ENOENT:0;
}



int boxitfs::readdir(const std::filesystem::path &path, std::any buffer, fuse_fill_dir_t filler, off_t offset, fuse_file_info &fi, fuse_readdir_flags flags)
{
    auto node = get_node(path);
    if(!node){
        return -ENOENT;
    }

    auto dirs = node->readdir();
    if(!dirs){
        return dirs.error();
    }
    filler(std::any_cast<void*>(buffer), ".", nullptr, 0, fuse_fill_dir_flags());
    filler(std::any_cast<void*>(buffer), "..", nullptr, 0, fuse_fill_dir_flags());

    for (const auto& dir : *dirs)
    {
        auto st = get_node(dir)->getattr();
        filler(std::any_cast<void*>(buffer), dir.c_str(), &*st, 0, fuse_fill_dir_flags::FUSE_FILL_DIR_PLUS);
    }

    return 0;

}

int boxitfs::write(const std::filesystem::path &path, std::any buffer, size_t size, off_t offset, fuse_file_info &fi)
{
    auto node = get_node_fi(fi);

    if(!node){
       node = std::shared_ptr<node_base>(get_node(path));
    }

    if(!node){
        return -ENOENT;
    }

    std::stringstream str;
    str.write(std::any_cast<const char*>(buffer), size);

    return node->write(str, offset);
}

int boxitfs::unlink(const std::filesystem::path &path)
{
    auto node = get_node(path);

    if(!node){
        return -ENOENT;
    }

    return node->unlink();
}
int boxitfs::release(const std::filesystem::path& path, fuse_file_info &fi)
{
    auto node = get_node_fi(fi);

    if(!node){
       node = get_node(path);
    }
    
    if(!node){
        return -ENOENT;
    }

    m_handles.erase(fi.fh);

    return node->release();
}

int boxitfs::access(const std::filesystem::path& path, int type)
{
    auto node = get_node(path);

    if(!node){return -ENOENT;}

    return node->access(type);
}


int boxitfs::statfs([[maybe_unused]] const std::filesystem::path& path,struct statvfs& sfs)
{
    sfs.f_namemax = 255;
    sfs.f_frsize = sfs.f_bsize = 1024;
    sfs.f_bavail = sfs.f_bfree = sfs.f_blocks 
        = 1000ULL * 1024 * 1024 * 1024 / sfs.f_frsize;
    sfs.f_favail = 1000;
    sfs.f_files = sfs.f_ffree = 1000000000;
    
    return 0;
}


std::shared_ptr<node_base> boxitfs::get_node(const std::filesystem::path &path)
{

    std::vector<std::string> path_parts;
     for(auto& part : path){
         path_parts.emplace_back(part);
     }
     if(path_parts.back()=="."){
         path_parts.pop_back();
     }
     if(path_parts.back()==".."){
         path_parts.erase(std::prev(path_parts.end(),2), path_parts.end());
     }
     std::shared_ptr<node_base> node;


     switch (path_parts.size()) {
     case 1:
         node = std::make_shared<node_root>();
         break;
     case 2:
         node = std::make_shared<node_branch>(path_parts[1]);
         break;
     case 3:
         node = std::make_shared<node_repo>(path_parts[1],path_parts[2]);
         break;
    case 4:
        if(path_parts[3].ends_with(".pkg.tar.zst")
            || path_parts[3].ends_with(".pkg.tar.xz")){
            node = std::make_shared<node_package>(path_parts[1],path_parts[2],path_parts[3]);
        } else if (path_parts[3].ends_with(".pkg.tar.zst.sig")
            || path_parts[3].ends_with(".pkg.tar.xz.sig")){
            node = std::make_shared<node_signature>(path_parts[1],path_parts[2],path_parts[3]);
        } else {
            return nullptr;
        }
        break;
     default:
         return nullptr;
     }
     node->set_channel(m_channel);



     return node;

}

void boxitfs::set_сhannel(const std::shared_ptr<grpc::Channel> &channel)
{
    m_channel = channel;
}


