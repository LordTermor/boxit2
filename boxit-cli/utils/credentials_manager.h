#pragma once
#include "credentials.h"
#include <optional>
#include <memory>
#include <settings_manager.h>

namespace global{
    extern std::string current_profile;
}

class credentials_manager {
    template<typename T> using observer_ptr = T*;
public:
    
    credentials_manager(settings_manager& mgr):m_manager(&mgr) {
        
    }
    
    class storage_base {
    public:
        virtual ~storage_base() = default;
        virtual bool store(const std::string& password, const std::string& profile_name) = 0;
        virtual std::optional<std::string> load( const std::string& profile_name) = 0;
        virtual bool clear(const std::string& profile_name) = 0;
        
    };
    struct storage_factory {
        virtual ~storage_factory() = default;
        virtual storage_base* create() = 0;
    };
    
    bool save(const credentials& creds, const std::string& profile_name = global::current_profile);
    std::optional<credentials> get(const std::string& profile_name = global::current_profile);
    bool reset(const std::string& profile_name = global::current_profile);
    
    void set_backend(storage_factory* factory);
protected:
    observer_ptr<settings_manager> m_manager;
    std::optional<credentials> get_profile(const std::string& name);
    std::unique_ptr<storage_base> m_storage;
};
class credentials_manager_cli : public credentials_manager {
public:
    credentials_manager_cli(settings_manager& mgr):credentials_manager(mgr){}
    credentials profile_prompt(const std::string& profile_name = global::current_profile);
private:
    std::string prompt(const std::string& description, bool allow_empty = false, bool echo = true);
};
