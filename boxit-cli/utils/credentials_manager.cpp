#include "credentials_manager.h"
#include <fmt/format.h>
#include <fmt/color.h>
#include <iostream>
#include <termios.h>
#include <unistd.h>

std::optional<credentials> credentials_manager::get(const std::string& profile_name)
{
    auto creds = get_profile(profile_name);
    creds->password = m_storage->load(profile_name);
    return creds;
}
bool credentials_manager::reset(const std::string& profile_name)
{
    return m_storage->clear(profile_name);
}

bool credentials_manager::save(const credentials& creds, const std::string& profile_name)
{
    auto res = m_manager->set({"profiles", profile_name, "username"}, creds.username);
    res &= m_manager->set({"profiles", profile_name, "host"}, creds.host);
        
    if(creds.password){
        res &= m_storage->store(creds.password.value(),profile_name);
    }
    m_manager->save();
    
    return res;
}

std::optional<credentials> credentials_manager::get_profile(const std::string& name)
{
    auto node = m_manager->get<YAML::Node>({"profiles", name});
    if(!(*node)["username"].IsDefined() && !(*node)["host"].IsDefined()){
        return {};
    }
    
    return credentials {
        .host = (*node)["host"].as<std::string>(""),
        .username = (*node)["username"].as<std::string>("")
    };
}

void credentials_manager::set_backend(credentials_manager::storage_factory* factory)
{
    m_storage = std::unique_ptr<storage_base>(factory->create());
}
credentials credentials_manager_cli::profile_prompt(const std::string& profile_name)
{
    auto creds = get(profile_name);
    
    if(!creds){
        creds = credentials{};
    }
    if(creds->host.empty()){
        creds->host =  prompt(fmt::format("Hostname{}", fmt::format(fmt::emphasis::italic, "<:port>")));
    }
    if(creds->username.empty()){
        creds->username = prompt("User");
    } 
    if (!creds->password){
        creds->password = prompt(fmt::format("{}{}", 
                                             fmt::format(fmt::emphasis::underline, creds->username),
                                             fmt::format(fmt::emphasis::bold, "'s password")
        ),false,false);
    }
    save(*creds);
    return *creds;
    
}
/* https://stackoverflow.com/questions/3276546/how-to-implement-getch-function-of-c-in-linux */
/* reads from keypress, doesn't echo */
int getch(void)
{
    struct termios oldattr, newattr;
    int ch;
    tcgetattr( STDIN_FILENO, &oldattr );
    newattr = oldattr;
    newattr.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );
    return ch;
}


static std::string replace_character = "•";
//Reads console's input replacing input characters with replace_character
static constexpr auto read_console_no_echo = []()->std::string{
    std::string value;
    while(true){
        std::cout.flush();
        char ch = getch();
        if(ch == 127){
            if(!value.empty()){
                value.resize(value.length()-1);
                std::cout<<"\b \b";
            }
        } else if(ch == '\n'){
            if(!value.empty()){
                std::cout<<"\n";
                break;
            }
        } else if (std::isalnum(ch)) {
            value += ch;
            std::cout<<replace_character;
        }
    }
    return value;
};
static constexpr auto read_console_echo = []()-> std::string {std::string value; std::cin >> value; return value;};
std::string credentials_manager_cli::prompt(const std::string& description, bool allow_empty, bool echo) {
    std::string value;
    auto read_console = echo?read_console_echo:read_console_no_echo;
    do {
        fmt::print(fmt::emphasis::bold|fg(fmt::terminal_color::green), " ? ");
        fmt::print(fmt::emphasis::bold, " {} : ", description);
        value = read_console();
    } while (!allow_empty && value.empty());
    
    return value;
}
