#include "secret.h"
#include <global.h>


bool secret::store()
{
    GError* err = nullptr;
    secret_password_store_sync(&PASSWORD_SCHEMA,SECRET_COLLECTION_DEFAULT,
                               "BoxIt2 profile password",
                               password.c_str(),nullptr,&err,
                               "profile", profile.c_str(),
                               nullptr);
    
    if(err!=nullptr){
        //can't store for some reason
        g_error_free(err);
        return false;
    }
    
    return true;
    
}

std::optional<secret> secret::lookup(const std::string &profile)
{
    GError* err = nullptr;
    
    auto pwd = secret_password_lookup_sync(&PASSWORD_SCHEMA, nullptr, &err, "profile", profile.c_str(), nullptr);
    
    
    if(err!=nullptr){
        return {};
    }
    if(pwd==nullptr){
        return {};
    }
    
    return secret {
        .profile = profile,
        .password = std::move(pwd)
    };
    
}

bool secret::remove(const std::string& name)
{
    GError* err = nullptr;
    
    auto removed = secret_password_clear_sync(&PASSWORD_SCHEMA,nullptr, &err, "profile", name.c_str(), nullptr);
    
    return err == nullptr && removed;
}

std::optional< std::string > credentials_manager_storage_secret::load(const std::string& profile) {
    
    auto sct = secret::lookup(profile);
    
    if(!sct){
        return {};
    }
    
    return sct->password;
    
}

bool credentials_manager_storage_secret::store(const std::string& password, const std::string& profile) {
    secret sct{
        .profile = profile,
        .password = password
    };
    
    return sct.store();
}

bool credentials_manager_storage_secret::clear(const std::string& profile_name)
{
    return secret::remove(profile_name);
}
credentials_manager::storage_base * storage_secret_factory::create()
{
    return new credentials_manager_storage_secret();
}
