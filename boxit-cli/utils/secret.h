#pragma once
#include <libsecret/secret.h>
#include <string>
#include <optional>

#include "credentials_manager.h"


struct secret
{

    std::string profile;
    std::string password;

    bool store();
    static std::optional<secret> lookup(const std::string& profile);
    static bool remove(const std::string& profile);
private:
    constexpr static const SecretSchema PASSWORD_SCHEMA = {
        .name = "org.manjaro.BoxIt2.Password",
        .flags = SECRET_SCHEMA_NONE,
        .attributes = {
            {  "profile", SECRET_SCHEMA_ATTRIBUTE_STRING },
            
        }
    };
};

class credentials_manager_storage_secret : public credentials_manager::storage_base {
public:
    bool store(const std::string& password, const std::string & profile_name) override;
    bool clear(const std::string & profile_name) override;
    std::optional<std::string> load(const std::string & profile_name) override;    
};

struct storage_secret_factory : public credentials_manager::storage_factory  {
    virtual ~storage_secret_factory() = default;
    virtual credentials_manager::storage_base * create() override final;
};
