#pragma once
#include <string>
#include <fmt/printf.h>
#include <fmt/color.h>
#include <iostream>

namespace cli_io
{
void warn(const std::string& message);
void err(const std::string& message);
void info(const std::string& message);
std::string question(const std::string& message, bool allow_empty = true);


};

