#include "cli_io.h"
using namespace fmt;
void cli_io::info(const std::string &message)
{
    std::cout << message << "\n";
}

void cli_io::err(const std::string &message)
{
    std::cerr << format( emphasis::bold
                        | fg(terminal_color::bright_red)
                        | bg(terminal_color::white),
                        " ! " )
              << format( emphasis::bold, " "+message ) << "\n";
}

void cli_io::warn(const std::string &message)
{
    std::cout << format( emphasis::bold
                         | fg(terminal_color::bright_magenta)
                         | bg(terminal_color::white),
                         "wrn")
              << format( " "+message ) << "\n";
}

std::string cli_io::question(const std::string &message, bool allow_empty)
{
    std::string result;

    do{
        std::cout << fmt::format(
                         fmt::emphasis::bold
                         | fg(fmt::terminal_color::green),
                         " ? " )
                  << fmt::format(fmt::emphasis::bold, " "+message) << ": ";


        std::cin>>result;
    } while(!allow_empty && result.empty());
    return result;
}
