#include "global.h"
#include "boost/log/utility/setup.hpp"
#include <services/user_service.h>
#include <services/permissions_service.h>
#include <utils.h>


const char *initializer::initialization_error::what() const noexcept
{
    return message.c_str();
}

void initializer::find_scripts()
{
    if(!utils::get_script("repo-add")){
        throw initialization_error("repo-add script is not found or not marked as executable");
    }
    if(!utils::get_script("repo-remove")){
        throw initialization_error("repo-remove script is not found or not marked as executable");
    }
}

void initializer::setup_db(SQLite::Database& db) {
    db.exec(
        "CREATE TABLE IF NOT EXISTS users (name string PRIMARY KEY, password string);"
        "CREATE TABLE IF NOT EXISTS permissions (name string PRIMARY KEY REFERENCES users(name), "
        "permission string);");
    
    SQLite::Statement st(db, "select count(*) from users");
    if(st.executeStep()){
        if(st.getColumn(0).getInt()==0){
            user_service::add_user("BoxItAdmin", utils::sha256("ILikeCookies"));
            permissions_service::add_permission("BoxItAdmin", {"*"});
        }
    }
}

void initializer::setup_watcher(file_watcher& watcher) {
    watcher.add_watch(std::filesystem::absolute("config.yml"),[](inotify_event* e){
        if(e->mask & IN_MODIFY){
            BOOST_LOG_TRIVIAL(info) << "Reloading config...";
            
            global::settings.reload();
            
            global::repository.reload("./repositories.yml");
        }
    });
    
    watcher.add_watch(std::filesystem::absolute("repositories.yml"),[](inotify_event* e){
        if(e->mask & IN_MODIFY){
            BOOST_LOG_TRIVIAL(info) << "Reloading repositories...";
            
            global::repository.reload("./repositories.yml");
        }
    });
}

void initializer::setup_logger() {
    boost::log::add_common_attributes();
    boost::log::add_console_log(
        std::cout,
        boost::log::keywords::format = "[%TimeStamp%][%Severity%]: %Message%",
        boost::log::keywords::auto_flush = true
    );
    boost::log::add_file_log(
        "journal.log",
        boost::log::keywords::format = "[%TimeStamp%][%Severity%]: %Message%",
        boost::log::keywords::auto_flush = true);
    
}

initializer::initializer() {
    setup_logger();
    find_scripts();
    setup_db(global::database);
    setup_watcher(global::watcher);
}

