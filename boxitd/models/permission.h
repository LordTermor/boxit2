#pragma once
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <initializer_list>

class permission
{
public:
    permission(const std::string& permission_string);
    permission(const std::initializer_list<std::string>& list);

    std::string part(std::size_t index)const{return m_parts[index];};
    std::size_t parts_count()const{return m_parts.size();}
    std::vector<std::string> parts() const {return m_parts;};

    std::string string() const {return boost::join(m_parts,".");}


private:
    std::vector<std::string> m_parts;
};

using permission_check_predicate = std::function<bool(const permission&)>;
