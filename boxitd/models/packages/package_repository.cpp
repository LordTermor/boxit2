#include "package_repository.h"
#include <boost/range/iterator_range.hpp>
#include <boost/log/trivial.hpp>
#include <boost/process.hpp>
#include <thread>
#include <chrono>
#include <archive_reader.hpp>
#include <utils/pacman_db_parser.h>
#include "global.h"
#include <utils/mail/boxit_message.h>
#include <mailio/smtp.hpp>
#include <fmt/format.h>

#include <utils/db_access/access_commands/access_command_add.h>
#include <utils/db_access/access_commands/access_command_remove.h>
#include <utils/db_access/access_commands/access_command_replace.h>
#include <utils/db_access/access_commands/access_command_move.h>


package_repository::package_repository(const std::filesystem::__cxx11::path &repository_name)
    :m_paths(global::settings.get<std::string>("root").value_or("repository"))
{
    if(!std::filesystem::exists(m_paths.repo())){
        std::filesystem::create_directories(m_paths.repo());
    }
    if(!std::filesystem::exists(m_paths.overlay())){
        std::filesystem::create_directories(m_paths.overlay());
    }
    
    load_from_yaml(repository_name);
}

void send_msg(const branch_t& branch, const repo_t& repo, const std::vector<package>& pkgs){

    boxit_message msg;
    msg.branch(branch);
    msg.repository(repo);
    msg.arch(package_arch::x86_64); ///TODO: architecture support
    msg.user("");

    msg.new_packages(pkgs);

    mailio::smtp smtp("localhost", 25);
    smtp.authenticate("","",mailio::smtp::auth_method_t::NONE);

    smtp.submit(msg);

}

bool package_repository::sync(const user &usr)
{
    std::thread thread([=](){
        for (auto& branch : m_syncable_branches){
            
#pragma omp parallel for
            for( std::size_t i = 0; i < m_sources.size(); i++){
                
                auto it = m_sources.begin();
                std::advance(it, i);
                
                auto& repo = it->first;
                auto& sources = it->second;
                
                std::vector<boxit_message> msgs;
                
                BOOST_LOG_TRIVIAL(info) << "Started synchronizing \""<< repo << "\" ("<<sources.size()<<" sources)";
                
                for( auto& source : sources){
                    auto summary = download_pkgs(branch, repo, source.get());
                }
                

            }
        }
    });
    thread.detach();
    return true;
}

bool package_repository::sync(const std::string &repo)
{
    if(!m_repos.contains(repo)){
        throw std::invalid_argument("No such repository");
    }
    std::thread thread([=](){
        for (auto& branch : m_syncable_branches){
            auto& sources = m_sources[repo];
            BOOST_LOG_TRIVIAL(info) << "Started synchronizing \""<< repo << "\" ("<<sources.size()<<" sources)";

            for( auto& source : sources){
                download_pkgs(branch, repo, source.get());
            }
        }
        
    });
    thread.detach();
    return true;
}

bool package_repository::snap(const branch_t &source_branch,
                              const branch_t &target_branch,
                              const repo_t &repo,
                              const std::vector<std::string> &ignore_pkg_names)
{

    if(!m_branches.contains(source_branch)){
        throw std::invalid_argument("No such source branch");
    }
    if(!m_branches.contains(target_branch)){
        throw std::invalid_argument("No such target branch");
    }


    m_queue.enqueue(std::make_shared<access_command_move>(access_command_base::access_options<std::initializer_list<class package>>{
                                                                .storage = access_command_base::package_storage::AUTO,
                                                                .branch = source_branch,
                                                                .repo = repo,
                                                                .package_list = {},
                                                              .callback = [=](access_command_base::status st){

                                                                  if(st==access_command_base::status::FAILED){
                                                                      return;
                                                                  }
                                                                  if(m_branches.contains(target_branch)){
                                                                      m_table[target_branch][repo].clear();

                                                                  }
                                                                  update_entries(target_branch, repo);

                                                              },
                                                                .paths = m_paths
                                                            }, target_branch));
    

    return true;
}

branch_comparison_t package_repository::compare(const branch_t &source_branch, const branch_t &target_branch)
{
    if(!m_branches.contains(source_branch)){
        throw std::invalid_argument("No such source branch");
    }
    if(!m_branches.contains(target_branch)){
        throw std::invalid_argument("No such target branch");
    }

    branch_comparison_t result;
    
    for(const auto& repo_name : m_repos) {
        auto source_repository = m_table[source_branch][repo_name];
        auto target_repository = m_table[target_branch][repo_name];
        
        
        for(const auto& package : source_repository){
            result[package.name()].first = package.version();
        }
        for(const auto& package : target_repository){
            result[package.name()].second = package.version();
        }
    }
    
    return result;
}


void package_repository::add_branch(const std::string &name, bool syncable)
{
    m_branches.emplace(name);
    for(auto& repo : m_repos) {
        update_entries(name, repo);
    }
    if(syncable){
        m_syncable_branches.emplace(name);
    }
}

void package_repository::add_repo(const std::string &name)
{
    m_repos.emplace(name);
    
    for(auto& br : m_branches) {
        update_entries(br,name);
    }
}

void package_repository::add_source(const repo_t &repo,package_source_ptr ptr)
{
    m_sources[repo].emplace_back(std::move(ptr));
}

std::set<package> package_repository::get_pkgs(const branch_t &branch, const repo_t &repo)
{
    if(m_branches.contains(branch) && m_repos.contains(repo)){
        
        return {m_table[branch][repo].begin(), m_table[branch][repo].end()};
    } else {
        throw std::invalid_argument("No such branch or repository");
    }
    
}

void package_repository::reload(const std::filesystem::__cxx11::path &repository_name)
{
    m_paths = repository_paths(global::settings.get<std::string>("root").value_or("./repository"));
    load_from_yaml(repository_name);
}

bool package_repository::remove(const branch_t &branch, const repo_t &repo, const package &package)
{
    m_queue.enqueue(std::make_shared<access_command_remove>(access_command_base::access_options<std::initializer_list<class package>>{
                                                                .storage = access_command_base::package_storage::AUTO,
                                                                .branch = branch,
                                                                .repo = repo,
                                                                .package_list = {package},
                                                                .callback = [=](access_command_base::status st){
                                                                    if(st==access_command_base::status::FAILED){
                                                                        return;
                                                                    }
                                                                    auto found = std::find_if(m_table[branch][repo].begin(),m_table[branch][repo].end(),
                                                                                              [=](auto& pkg){
                                                                        return pkg.name()==package.name();
                                                                    });
                                                                    m_table[branch][repo].erase(found);
                                                                },
                                                                .paths = m_paths
                                                            }));



    return true;
}


std::ofstream package_repository::overlay_file_stream(const std::string& filename)
{
    return std::ofstream{m_paths.overlay() / filename, std::ios::binary | std::ios::out | std::ios::app};
}


bool package_repository::register_package(const branch_t& branch, const repo_t& repo, const package& package)
{
    auto relative_overlay = std::filesystem::relative(m_paths.overlay(), m_paths.terminal(branch,repo));

    
    auto found = std::find_if(m_table[branch][repo].begin(),m_table[branch][repo].end(),
                              [=](auto& pkg){
        return pkg.name()==package.name();
    });
    
    if(found!=m_table[branch][repo].end()){
        
        BOOST_LOG_TRIVIAL(info) << fmt::format("Changing {} version ({} over {})", package.name(), package.version().string(), found->version().string());


        m_queue.enqueue(std::make_shared<access_command_remove>(access_command_base::access_options<std::initializer_list<class package>>{
                                                                    .storage = access_command_base::package_storage::AUTO,
                                                                    .branch = branch,
                                                                    .repo = repo,
                                                                    .package_list = {*found},
                                                                    .callback = [=](access_command_base::status st){
                                                                        if(st==access_command_base::status::FAILED){
                                                                            return;
                                                                        }
                                                                        m_table[branch][repo].erase(found);
                                                                    },
                                                                    .paths = m_paths
                                                                }));        
    }
    

    m_queue.enqueue(std::make_shared<access_command_add>(access_command_base::access_options<std::initializer_list<class package>>{
                                                             .storage = access_command_base::package_storage::OVERLAY,
                                                             .branch = branch,
                                                             .repo = repo,
                                                             .package_list = {package},
                                                             .callback = [=](access_command_base::status st){
                                                                 if(st==access_command_base::status::FAILED){
                                                                     return;
                                                                 }
                                                                 m_table[branch][repo].emplace(package);
                                                             },
                                                             .paths = m_paths,
                                                         }));
    return true;
}


package_repository::summary package_repository::download_pkgs(const branch_t &branch, const repo_t &repo, package_source *source)
{
    using namespace std::chrono_literals;
    summary result;

    auto packages = source->get_avaiable_packages();
    BOOST_LOG_TRIVIAL(info) << "\"" << repo <<"\":"<<" source has " << packages.size() << " packages";
    
    for(auto& package : packages){
        

        if(m_table[branch][repo].contains(package))
        {
            BOOST_LOG_TRIVIAL(info) << fmt::format("{} is already in repository, skipping...", package.filename());
            continue;
        }
        if(m_ignores[repo].contains(package.name()) ||
                m_global_ignores.contains(package.name()))
        {
            BOOST_LOG_TRIVIAL(info) << fmt::format("{} is in ignore list, skipping...", package.name());
            continue;
        }
        
        std::this_thread::sleep_for(1s);
        
        
        if(!std::filesystem::exists(m_paths.repo_package(package.filename()))){
            source->download_to_pool(package.filename(), m_paths.repo());
        }
        
        auto found = std::find_if(m_table[branch][repo].begin(),m_table[branch][repo].end(),
                                  [=](auto& pkg){
            return pkg.name()==package.name();
        });
        
        if(found != m_table[branch][repo].end()){
            result.removed.emplace_back(*found);
        }
        
        result.added.emplace_back(package);
    }
    m_queue.enqueue(std::make_shared<access_command_remove>(access_command_base::access_options<std::vector<class package>>{
                                                                .storage = access_command_base::package_storage::AUTO,
                                                                .branch = branch,
                                                                .repo = repo,
                                                                .package_list = result.removed,
                                                                .callback = [=](access_command_base::status st){
                                                                    if(st==access_command_base::status::SUCCESS){
                                                                        send_msg(branch, repo, result.added);

                                                                        for(const auto& pkg : result.removed){
                                                                            m_table[branch][repo].erase(pkg);
                                                                        }

                                                                    }
                                                                },
                                                                .paths = m_paths
                                                            }));
    m_queue.enqueue(std::make_shared<access_command_add>(access_command_base::access_options<std::vector<class package>>{
                                                             .storage = access_command_base::package_storage::REPO,
                                                             .branch = branch,
                                                             .repo = repo,
                                                             .package_list = result.added,
                                                             .callback = [=](access_command_base::status st){
                                                                 if(st==access_command_base::status::SUCCESS){
                                                                     send_msg(branch, repo, result.added);
                                                                     BOOST_LOG_TRIVIAL(info) << "Synchronization of \"" << repo << "\" is done";


                                                                     for(const auto& pkg : result.added){
                                                                         m_table[branch][repo].emplace(pkg);
                                                                     }

                                                                 } else {
                                                                     BOOST_LOG_TRIVIAL(info) << "Synchronization of \"" << repo << "\" is failed";
                                                                 }
                                                             },
                                                             .paths = m_paths,

                                                         }));
    return result;
}



void package_repository::update_entries(const branch_t& br,const repo_t& repo)
{
    
    m_table.emplace(br,repo, std::set<package>());
    if (std::filesystem::exists(m_paths.terminal(br,repo))){
        if(std::filesystem::exists(m_paths.database(br,repo).string()+".tar.xz")){
            
            moor::ArchiveReader reader(m_paths.database(br,repo).string()+".tar.xz");
            auto data = reader.ExtractNext();
            
            while(data.first.length() > 0)
            {
                
                if(boost::ends_with(data.first, "/desc")){
                    
                    pacman_db_parser parser{{reinterpret_cast<char*>(data.second.data())}};
                    
                    m_table[br][repo].emplace(package{
                                                  parser["FILENAME"]
                                              });
                }
                data = reader.ExtractNext();
            }
        }
        BOOST_LOG_TRIVIAL(info) << fmt::format("{}/{} has {} packages", br, repo, m_table[br][repo].size());
    } else {
        std::filesystem::create_directories(m_paths.terminal(br,repo));
    }
}

void package_repository::load_from_yaml(const std::filesystem::__cxx11::path &filename)
{
    m_branches.clear();
    m_repos.clear();
    
    m_table.clear();
    
    try{
        auto node = YAML::LoadFile(filename);
        
        auto branches = global::settings.get<std::vector<std::string>>("branches");
        
        for(const auto &branch : branches.value_or(std::vector<std::string>())){
            if(branch=="unstable"){
                add_branch(branch,true);
            } else {
                add_branch(branch);
            }
        }
        auto glob_ignores = global::settings.get<YAML::Node>("ignores");
        
        for(const auto& ignores : glob_ignores.value_or(YAML::Node())) {
            import_ignores(ignores);
        }
        
        for (const auto &repo : node){
            
            add_repo(repo.first.as<std::string>());
            
            for (const auto& source : repo.second["sources"]){
                if(source["type"].as<std::string>()=="repository"){
                    add_source(repo.first.as<std::string>(),
                               std::unique_ptr<package_source>(repo_source_factory().create_from_node(source)));
                }
            }
            for (const auto& ignores : repo.second["ignores"]){
                import_ignores(ignores, repo.first.as<std::string>());
            }
        }
    } catch (YAML::BadFile& exception){
        
    }
    if(m_repos.size()==0 || m_branches.size()==0){
        BOOST_LOG_TRIVIAL(warning) << "No branches or repositories are configured, BoxIt2 server will be malfunctioned. Check your settings.";
    }
}

std::set< std::string > package_repository::get_signatures(const branch_t& branch, const repo_t& repo)
{
    std::set<std::string> result;
    for(const auto& package : m_table[branch][repo]){
        if(std::filesystem::exists(m_paths.signature(branch,repo,package.filename()))){
            result.insert(package.filename()+".sig");
        }
    }
    return result;
}

void package_repository::import_ignores(const YAML::Node& nd, const std::string& repo)
{
    
    auto& ignorelist = repo.empty()?m_global_ignores:m_ignores[repo];
    try{
        if(nd.IsMap()){
            if(nd["type"].as<std::string>()!="file"){
                return;
            }
            std::ifstream str(nd["path"].as<std::string>());
            
            std::string ignore;
            int num_of_ignores = 0;
            
            while(std::getline(str,ignore)){
                
                boost::trim(ignore);
                
                if(ignore.empty() || ignore.starts_with("#")){
                    continue;
                }
                
                ignorelist.insert(ignore);
                num_of_ignores++;
            }
            BOOST_LOG_TRIVIAL(info)<<fmt::format("Imported ignore list \"{}\" with {} ignored packages for {}", nd["path"].as<std::string>(), num_of_ignores, repo.empty()?"global":repo);
        }
        else if (nd.IsSequence()){
            for(const auto& ignore : nd){
                ignorelist.insert(ignore.as<std::string>());
            }
        }
    } catch (std::exception& e){}
    
}
