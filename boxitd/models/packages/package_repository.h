#pragma once
#include <unordered_map>
#include <string>
#include <memory>
#include <models/packages/sources/repo_source.h>
#include <models/packages/sources/package_source.h>
#include "double_map.h"
#include <filesystem>
#include <types.h>
#include <utils/repository_paths.h>
#include <mutex>
#include <user.h>
#include <utils/access_queue.h>

namespace std {

  template <>
  struct hash<std::pair<branch_t, repo_t>>
  {
    std::size_t operator()(const std::pair<branch_t, repo_t>& k) const
    {
      using std::size_t;
      using std::hash;
      using std::string;

      return ((hash<string>()(k.first)
               ^ (hash<string>()(k.second) << 1)) >> 1);
    }
  };

}

class package_repository
{
public:
    package_repository(const std::filesystem::path& repository_name);
    
    bool sync(const repo_t& repo);
    bool sync(const user& usr = user());

    bool snap(const branch_t& source_branch,
              const branch_t& target_branch,
              const repo_t& repo,
              const std::vector<std::string>& ignore_pkg_names);

    branch_comparison_t compare(const branch_t& source_branch,
                                const branch_t& target_branch);

    void add_branch(const std::string& name, bool syncable = false);
    void add_repo(const std::string& name);
    void add_source(const repo_t& repo,package_source_ptr ptr);

    std::set<package> get_pkgs(const branch_t& branch, const repo_t& repo);
    std::set<std::string> get_signatures(const branch_t& branch, const repo_t& repo);

    std::set<branch_t> branches() const
    {
        return m_branches;
    }

    std::set<repo_t> repos() const
    {
        return m_repos;
    }
    

   void reload(const std::filesystem::path& repository_name);

   bool remove(const branch_t& branch, const repo_t& repo, const package& package);

   std::ofstream overlay_file_stream(const std::string& filename);
   bool register_package(const branch_t& branch, const repo_t& repo, const package& package);


private:

   struct summary{
       std::vector<package> removed;
       std::vector<package> added;
   };

    /* +-------------+------+-----------+-----
     * | branch\repo | core | community | ...
     * +-------------+------+-----------+-----
     * |   unstable  | pkgs |   pkgs    | ...
     * +-------------+------+-----------+-----
     * |   testing   | pkgs |   pkgs    | ...
     * +-------------+------+-----------+-----
     * |    ...      | ...  |   ...     | ...
     * +-------------+------+-----------+-----
    */
    double_map<branch_t, repo_t, std::set<package>> m_table;


    std::unordered_map<repo_t, std::vector<package_source_ptr>> m_sources;

    
    std::unordered_map<repo_t, std::set<std::string>> m_ignores;
    std::set<std::string> m_global_ignores;
    
    std::set<branch_t> m_syncable_branches;

    std::set<branch_t> m_branches;
    std::set<repo_t> m_repos;

    summary download_pkgs(const branch_t& br,const repo_t& repo, package_source*);
    void update_entries(const branch_t& br,const repo_t& repo);
    void load_from_yaml(const std::filesystem::path& filename);
    void import_ignores(const YAML::Node& nd,const std::string& repo = "");

    repository_paths m_paths;
    access_queue m_queue;


};
