#pragma once
#include <string>
#include <vector>
#include "package.h"
#include <filesystem>
#include <yaml-cpp/yaml.h>
class package_source
{
public:
    package_source() = default;
    virtual ~package_source() = default;
    virtual void download_to_pool(const std::string& package_name, const std::filesystem::path& pool_path) = 0;
    virtual std::vector<package> get_avaiable_packages() = 0;

};


#include <memory>
using package_source_ptr = std::unique_ptr<package_source>;


class package_source_factory {
public:
    virtual package_source* create_from_node(const YAML::Node&) = 0;
};
