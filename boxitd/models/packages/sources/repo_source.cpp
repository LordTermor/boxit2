#include "repo_source.h"


#include <utils/pacman_db_parser.h>


#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include "global.h"

#include <archive_reader.hpp>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/algorithm/string.hpp>
#include <Url.h>

#include <boost/log/trivial.hpp>

namespace beast = boost::beast;
namespace http = beast::http;
namespace net = boost::asio;
namespace ssl = net::ssl;
using tcp = net::ip::tcp;

repo_source::repo_source(const std::string& name, const repo_source::endpoint &ep):m_ep(ep),m_name(name),m_ctx(ssl::context::tlsv12_client)
{
    m_ctx.set_default_verify_paths();
    m_ctx.set_verify_mode(ssl::verify_peer);
}

repo_source::repo_source():m_ctx(ssl::context::tlsv12_client)
{
    m_ctx.set_default_verify_paths();
    m_ctx.set_verify_mode(ssl::verify_peer);
}

std::string repo_source::name() const
{
    return m_name;
}

void repo_source::set_name(const std::string &value)
{
    m_name = value;
}

repo_source::endpoint repo_source::get_endpoint() const
{
    return m_ep;
}

void repo_source::set_endpoint(const endpoint &ep)
{
    m_ep = ep;
}

void repo_source::download(const std::string &req_path, const std::filesystem::path& path){
    net::io_context ioc;

    tcp::resolver resolver(ioc);
    beast::ssl_stream<beast::tcp_stream> stream(ioc, m_ctx);

    SSL_set_tlsext_host_name(stream.native_handle(), m_ep.host.c_str());

    auto const results = resolver.resolve(m_ep.host, std::to_string(m_ep.port));

    beast::get_lowest_layer(stream).connect(results);
    stream.handshake(ssl::stream_base::client);

    http::request<http::string_body> req
    {
        http::verb::get,
                req_path,
                10
    };
    req.set(http::field::host, m_ep.host);
    req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

    http::write(stream, req);

    beast::flat_buffer buffer;

    http::response_parser<http::file_body> res;

    res.body_limit(std::numeric_limits<uint64_t>::max());
    beast::error_code ec;
    res.get().body().open(path.c_str(),beast::file_mode::write,ec);

    BOOST_LOG_TRIVIAL(info) <<"Downloading "<<req_path<<"... ";


    http::read(stream, buffer, res);

    BOOST_LOG_TRIVIAL(info) <<"Downloaded "<<req_path << " as " << path;
    stream.shutdown(ec);

    if(ec && ec != beast::errc::not_connected)
        throw beast::system_error{ec};

    res.get().body().close();
}

void repo_source::download_to_pool(const std::string &package_name, const std::filesystem::path &pool)
{
    BOOST_LOG_TRIVIAL(info) << "Getting package " << package_name << " from repository " << m_ep.string();
    download(m_ep.target+"/"+package_name,pool/package_name);

    BOOST_LOG_TRIVIAL(info) << "Trying to get .sig file " << (package_name + ".sig") << " from repository " << m_ep.string();

    download(m_ep.target+"/"+package_name+".sig", pool/(package_name+".sig"));
}

std::vector<package> repo_source::get_avaiable_packages()
{

    std::vector<package> result;

    net::io_context ioc;


    tcp::resolver resolver(ioc);
    beast::ssl_stream<beast::tcp_stream> stream(ioc, m_ctx);

    SSL_set_tlsext_host_name(stream.native_handle(), m_ep.host.c_str());

    auto const results = resolver.resolve(m_ep.host, std::to_string(m_ep.port));

    beast::get_lowest_layer(stream).connect(results);

    stream.handshake(ssl::stream_base::client);

    http::request<http::string_body> req
    {
        http::verb::get,
                m_ep.target + "/" + m_name+".db",
                10
    };
    req.set(http::field::host, m_ep.host);
    req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

    http::write(stream, req);

    beast::flat_buffer buffer;

    http::response<http::vector_body<uint8_t>> res;

    http::read(stream, buffer, res);
    BOOST_LOG_TRIVIAL(info) << "Getting database file " << (m_ep.target + "/" + m_name+".db") << "...";

    beast::error_code ec;
    stream.shutdown(ec);

    if(ec && ec != beast::errc::not_connected)
        throw beast::system_error{ec};

    moor::ArchiveReader reader(res.body().data(), res.body().size());
    auto data = reader.ExtractNext();

    while(data.first.length() > 0)
    {

        if(boost::ends_with(data.first, "/desc")){

            pacman_db_parser parser{{reinterpret_cast<char*>(data.second.data())}};

            result.emplace_back(package{
                                    parser["FILENAME"]
                                });
        }
        data = reader.ExtractNext();
    }
    return result;
}


package_source* repo_source_factory::create_from_node(const YAML::Node &node)
{
    auto result = new repo_source;

    result->set_name(node["name"].as<std::string>());

    repo_source::endpoint ep;
    homer6::Url url(node["url"].as<std::string>());
    ep.host = url.getHost();
    ep.port = url.getPort()==0?443:url.getPort();
    ep.target = url.getPath();

    result->set_endpoint(ep);

    return result;

}
