#pragma once
#include "package_source.h"

class ci_source: public package_source
{
public:
    ci_source();

    // package_source interface
public:
    void download_to_pool(const std::string &package_name, const std::filesystem::path& pool) override;
    std::vector<package> get_avaiable_packages() override;
};
