#include "permission.h"
permission::permission(const std::string &permission_string)
{
    boost::split(m_parts, permission_string, boost::is_any_of("."));
}

permission::permission(const std::initializer_list<std::string> &list)
{
    m_parts.resize(list.size());
    std::copy(list.begin(),list.end(), m_parts.begin());
}
