#include <boost/asio.hpp>
#include "boost/log/trivial.hpp"
#include "server.h"
#include <iostream>
#include "global.h"
#include <filesystem>
#include <utils/file_watcher.h>
#include <utils.h>
#include <services/user_service.h>
#include <services/permissions_service.h>

int main()
{

    try
    {
        boost::asio::io_context io_context;

        server server(global::settings.get<int>("port").value_or(5000));

        server.start();
    }
    catch (const std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << e.what();
        return 1;
    }

    return 0;
}
