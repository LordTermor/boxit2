#include "server.h"
#include <thread>
#include <protos/auth.pb.h>
#include <boost/asio.hpp>
#include <ostream>
#include <controllers/router.h>
#include <boost/log/trivial.hpp>
#include <utils.h>
#include <grpc++/grpc++.h>
#include <fstream>


class Auth : public grpc::AuthMetadataProcessor {


    // AuthMetadataProcessor interface
public:
    virtual grpc::Status Process(const InputMetadata &auth_metadata, grpc::AuthContext *context, OutputMetadata *consumed_auth_metadata,
                                 [[maybe_unused]] OutputMetadata *response_metadata) override
    {

        auto username_it = auth_metadata.find("username");
        auto password_it = auth_metadata.find("password");
        if (username_it == auth_metadata.end() || password_it == auth_metadata.end()){
            BOOST_LOG_TRIVIAL(info) <<"Missing Credentials";
            return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "Missing Credentials");
        }

        std::string username {username_it->second.begin(), username_it->second.end()};
        std::string password {password_it->second.begin(), password_it->second.end()};

        if (!auth_service::authorize(username, password)){
            BOOST_LOG_TRIVIAL(info) << "Credentials do not match";
            return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "Credentials do not match");
        }

        consumed_auth_metadata->insert({"username", username});
        consumed_auth_metadata->insert({"password", password});

        context->AddProperty("username", username);
        context->SetPeerIdentityPropertyName("username"); 

        return grpc::Status::OK;
    }
};

inline std::string load_cert(const std::string& key_cert_file){

    auto path = global::settings.get<std::string>("certs_path").value_or("certs") + "/" + key_cert_file;
    std::ifstream cert_file(path);

    return {std::istreambuf_iterator<char>(cert_file), std::istreambuf_iterator<char>()};

}

void server::start()
{
    auto endpoint = "0.0.0.0:"+std::to_string(m_port);
    BOOST_LOG_TRIVIAL(info) << "\n" << utils::box;


    grpc::ServerBuilder builder;


    std::string servercert = load_cert("server.crt");
    std::string serverkey = load_cert("server.key");

    grpc::SslServerCredentialsOptions::PemKeyCertPair pkcp;
    pkcp.private_key = serverkey;
    pkcp.cert_chain = servercert;

    grpc::SslServerCredentialsOptions ssl_opts;
    ssl_opts.pem_root_certs="";
    ssl_opts.pem_key_cert_pairs.push_back(pkcp);

auto creds = grpc::SslServerCredentials(ssl_opts);

    creds->SetAuthMetadataProcessor(std::shared_ptr<class Auth>(new class Auth));

    builder.AddListeningPort(endpoint, creds);
    builder.RegisterService(new package_controller);
    builder.RegisterService(new fs_controller);
    builder.RegisterService(new user_controller);
    builder.RegisterService(new log_controller);

    BOOST_LOG_TRIVIAL(info) << "Started BoxIt2 server at " << endpoint;

    auto server = builder.BuildAndStart();
    

    server->Wait();
    
    //     while(true){
    //
    //         auto socket = std::make_unique<boost::asio::ip::tcp::socket>(m_acceptor.accept());
    //         BOOST_LOG_TRIVIAL(info) << "Incoming connection from: "<<socket->remote_endpoint();
    //         boost::asio::post(m_pool, [socket{std::move(socket)}](){
    //
    //
    //
    //             auto ep = socket->remote_endpoint();
    //
    //             boost::system::error_code ec;
    //
    //             std::array<std::size_t, 1> incoming_buffer_size;
    //             boost::asio::read(*socket, boost::asio::buffer(incoming_buffer_size));
    //
    //             boost::asio::streambuf in_msg;
    //             std::istream stream(&in_msg);
    //
    //             auto prepared_buffer = in_msg.prepare(incoming_buffer_size[0]);
    //
    //
    //             int n = boost::asio::read(*socket, prepared_buffer, ec);
    //             if(ec && ec != boost::asio::error::eof){
    //                 return;
    //             }
    //             in_msg.commit(n);
    //
    //             boost::asio::streambuf out_msg;
    //
    //             std::ostream ostream(&out_msg);
    //             int msgType;
    //             stream >> msgType;
    //
    //             if(msgType == 0){
    //                 BOOST_LOG_TRIVIAL(info) << ep << " asked for version";
    //
    //                 ostream << BOXIT_VERSION;
    //
    //             } else {
    //
    //                 BOOST_LOG_TRIVIAL(info) << ep << " sent message type " << msgType;
    //
    //                 router::routes.at(msgType)(stream,ostream);
    //             }
    //
    //             boost::asio::write(*socket, out_msg);
    //
    //             socket->close();
    //             BOOST_LOG_TRIVIAL(info) << "Successfully closed connection from "<<ep;
    //
    //         });
    //     }
}
