#include "package_service.h"
#include "global.h"

bool package_service::sync(const std::string &repo)
{

    if(repo.empty()){
        return global::repository.sync();
    } else {
        return global::repository.sync(repo);
    }
}

bool package_service::snap(const branch_t &from, const branch_t &to)
{
    bool result = true;
    for(const auto& repo:global::repository.repos()){
        result &= global::repository.snap(from,to,repo,{});
    }
    return result;
}

branch_comparison_t package_service::compare(const branch_t &source, const branch_t &target)
{
    return global::repository.compare(source,target);
}

std::set<branch_t> package_service::branches()
{
    return global::repository.branches();
}
