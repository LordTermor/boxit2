#pragma once
#include <string>
#include <SQLiteCpp/Database.h>
#include "global.h"
class auth_service
{
public:
    auth_service();

    static bool authorize(const std::string& name,
                   const std::string& pwd);
};
