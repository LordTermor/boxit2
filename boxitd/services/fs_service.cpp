#include "fs_service.h"
#include <global.h>
#include <fstream>
#include <boost/log/trivial.hpp>
fs_service::fs_service()
{

}

std::set<branch_t> fs_service::get_branches()
{
    return global::repository.branches();
}

std::set<repo_t> fs_service::get_repos(const branch_t &branch)
{
    return global::repository.repos();
}

std::set<std::string> fs_service::get_packages(const branch_t &branch, const repo_t &repo)
{
    //TODO maybe do as different grpc endpoints?
    auto pkgs = global::repository.get_pkgs(branch,repo);
    
    auto files = global::repository.get_signatures(branch,repo);
    
    std::transform(pkgs.begin(), pkgs.end(), std::inserter(files,files.end()), [](const package& pkg){return pkg.filename();});
    
    return files;
}

bool fs_service::write(const std::string &filename, off_t offset, ::grpc::ServerReader<WriteRequest> *reader)
{
    auto stream = global::repository.overlay_file_stream(filename);


    stream.seekp(offset, std::ios::beg);

    int64_t size = 0;

    WriteRequest rq;
    while(reader->Read(&rq)){

        if(rq.content_case()!=WriteRequest::ContentCase::kDataBlock){
            BOOST_LOG_TRIVIAL(info) << "You should read metadata first";
            return false;
        }

        stream.write(rq.data_block().data().data(),rq.data_block().size());

        size+=rq.data_block().size();

    }

    stream.close();




    return true;
}

bool fs_service::register_package(const branch_t& branch, const repo_t& repo, const package& pkg)
{
    return global::repository.register_package(branch,repo,pkg);
}


std::ostream fs_service::read(const package &pkg)
{

}

bool fs_service::remove(const branch_t &branch, const repo_t &repo, const package &pkg)
{
    BOOST_LOG_TRIVIAL(info) << "Removing " << branch << "/" << repo << "/" << pkg.filename();

    return global::repository.remove(branch, repo, pkg);


}
bool fs_service::exists(const branch_t& branch, const repo_t& repo, const std::string& pkg, bool signature)
{
    if(!branch.empty() && !repo.empty() && !pkg.empty()){

        if(signature){
            auto signs = global::repository.get_signatures(branch, repo);
            return signs.contains(pkg);
        } else {
            auto pkgs = global::repository.get_pkgs(branch, repo);
            return pkgs.contains(pkg);
        }
    } 
    
    if (!branch.empty() && !repo.empty()){
        return global::repository.repos().contains(repo);
    }
    
    if (!branch.empty()){
        return global::repository.branches().contains(branch);
    } 
    
    return false;
}
