#include "user_service.h"
#include "global.h"
#include <SQLiteCpp/Transaction.h>
#include <boost/log/core.hpp>
#include <iostream>
#include <boost/log/trivial.hpp>
user_service::user_service()
{

}

bool user_service::add_user(const std::string &name, const std::string &password)
{
    BOOST_LOG_TRIVIAL(info) << "Trying to add user: "<<name;
    try{
        SQLite::Transaction transaction(global::database);

        SQLite::Statement query(global::database, "INSERT INTO users VALUES (?,?)");

        query.bind(1, name);
        query.bind(2, password);


        bool result = query.exec()==1;

        transaction.commit();
        BOOST_LOG_TRIVIAL(info)<<"User "<< name << " added";

        return result;
    } catch(SQLite::Exception e){
        BOOST_LOG_TRIVIAL(error)<<"SQL Error: "<<e.what();
        return false;
    }
}

bool user_service::remove_user(const std::string &name)
{
    BOOST_LOG_TRIVIAL(info) << "Trying to remove user: "<<name;

    try{
        SQLite::Transaction transaction(global::database);

        SQLite::Statement query(global::database, "DELETE FROM users WHERE name = ?");

        query.bind(1, name);

        bool result = query.exec()==1;

        transaction.commit();
        BOOST_LOG_TRIVIAL(info)<<"User "<< name << " removed";

        return result;
    } catch(SQLite::Exception e){
        std::cout<<"SQL Error: "<<e.what()<<"\n";
        return false;
    }
}

user user_service::get_user(const std::string &name)
{
    SQLite::Statement query(global::database, "SELECT * FROM users WHERE name = ?");

    query.bind(1, name);
    user usr;

    if(query.executeStep()){

        usr.name =  query.getColumn(0).getString();
        usr.password = query.getColumn(1).getString();
    }
    return usr;
}
