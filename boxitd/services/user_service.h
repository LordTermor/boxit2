#pragma once
#include <string>
#include <user.h>
class user_service
{
public:
    user_service();

    static bool add_user(const std::string& name, const std::string& password);
    static bool remove_user(const std::string& name);
    static user get_user(const std::string& name);
};
