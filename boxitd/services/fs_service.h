#pragma once
#include <types.h>
#include <vector>
#include <set>
#include <package.h>
#include <string>

#include <protos/filesystem.grpc.pb.h>

class fs_service
{
public:
    fs_service();

    static std::set<branch_t> get_branches();
    static std::set<repo_t> get_repos(const branch_t& branch);
    static std::set<std::string> get_packages(const branch_t& branch, const repo_t& repo);
    static bool write(const std::string &filename, off_t offset, ::grpc::ServerReader<WriteRequest> *reader);
    static bool register_package(const branch_t& branch, const repo_t& repo, const package& pkg);
    static std::ostream read(const package& pkg);
    static bool remove(const branch_t& branch, const repo_t& repo, const package& pkg);
    static bool exists(const branch_t& branch = "", const repo_t& repo = "", const std::string& pkg = "", bool signature = false);

};
