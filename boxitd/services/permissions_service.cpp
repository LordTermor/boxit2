#include "permissions_service.h"
#include <SQLiteCpp/SQLiteCpp.h>
#include <global.h>
#include <regex>
bool permissions_service::check(const std::string &name, const permission &perm)
{

    SQLite::Statement query(global::database, "select * "
                                             "from permissions "
                                             "where name = ? and "
                                             "(permission like replace(?,'*','%')"
                                             "or ? like replace(permission,'*','%'))");


    query.bind(1, name);
    query.bind(2, perm.string());
    query.bind(3, perm.string());

    return query.executeStep();
}

void permissions_service::add_permission(const std::string &name, const permission &perm)
{
    try{
        SQLite::Transaction transaction(global::database);

        SQLite::Statement query(global::database, "INSERT INTO permissions"
                                                  "VALUES (?,?)");

        query.bind(1, name);
        query.bind(2, perm.string());

        query.exec();
        transaction.commit();

    } catch(SQLite::Exception e){

    }
}

void permissions_service::remove_permission(const std::string &name, const permission &perm)
{
    try{
        SQLite::Transaction transaction(global::database);

        SQLite::Statement query(global::database, "DELETE FROM permissions "
                                                  "WHERE name = ? and "
                                                  "(permission like replace(?,'*','%')"
                                                  "or ? like replace(permission,'*','%'))");

        query.bind(1, name);
        query.bind(2, perm.string());
        query.bind(3, perm.string());

        query.exec();
        transaction.commit();

    } catch(SQLite::Exception e){
    }
}
