#pragma once
#include <queue>
#include <list>
#include <variant>
#include <memory>
#include <mutex>
#include <thread>
#include <types.h>
#include <package.h>
#include <utils/repository_paths.h>
#include <utils/db_access/access_commands/access_command_base.h>

class access_queue
{
public:
    access_queue() = default;

    bool enqueue(const std::shared_ptr<access_command_base>& request);


private:
    void process();

    bool repo_script(const std::string& script_path, const std::string& database, const std::string& package);
    bool repo_add(const branch_t& branch, const repo_t& repo, const package& pkg);
    bool repo_remove(const branch_t& branch, const repo_t& repo, const package& pkg);

    bool create_symlink(const branch_t& branch, const repo_t& repo, const package& pkg);
    bool remove_symlink(const branch_t& branch, const repo_t& repo, const package& pkg);

    std::atomic<bool> m_is_processing;
    std::mutex m_queue_mutex;
    std::queue<std::shared_ptr<access_command_base>, std::list<std::shared_ptr<access_command_base>>> m_queue;

};
