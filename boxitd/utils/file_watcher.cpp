#include "file_watcher.h"
#include <boost/log/trivial.hpp>
#include <unistd.h>
#include <poll.h>

//structure size * max filename size * max events at one time;
static const int buffer_size = sizeof (inotify_event)* 255 * 1024;

file_watcher::file_watcher():
    m_inotify_handle_fd(inotify_init1(IN_CLOEXEC))
{
    using namespace std::chrono_literals;

    m_main_loop_thread = std::thread{ [this](){

        pollfd pfd;
        pfd.fd = m_inotify_handle_fd;
        pfd.events = POLLIN;


        while(true){
            if(m_inotify_handle_fd == 0){
                break;
            }

            poll(&pfd, 1, std::chrono::duration_cast<std::chrono::milliseconds>(1s).count());
            if(!(pfd.revents & POLLIN)){
                continue;
            }
            char buffer[buffer_size];
            auto length = read( m_inotify_handle_fd, buffer, buffer_size );
            int i = 0;
            while ( i < length ) {
                inotify_event *event = ( inotify_event * ) &buffer[ i ];

                const auto& [string,
                        handler] = m_watch_handlers[event->wd];

                BOOST_LOG_TRIVIAL(info) << "FileWatcher :: " << string << " notified.";

                handler(event);
                i += sizeof (inotify_event) + event->len;
            }

            }
    }
};


}
file_watcher::~file_watcher()
{
    for(const auto &el : m_watch_handlers){
        inotify_rm_watch( m_inotify_handle_fd, el.first );
    }
    int temp_fd = m_inotify_handle_fd;
    m_inotify_handle_fd = 0;
    close(temp_fd);
    m_main_loop_thread.join();

}

void file_watcher::add_watch(const std::string &str, std::function<void(inotify_event *)> handler, uint32_t mask)
{
    int wd = inotify_add_watch(m_inotify_handle_fd, str.c_str(), mask);

    m_watch_handlers.emplace(wd, std::make_pair(str,handler));

}
