#pragma once
#include <sys/inotify.h>
#include <string>
#include <functional>
#include <thread>
#include <atomic>

class file_watcher
{
public:
    file_watcher();
    ~file_watcher();
    void add_watch(const std::string& str, std::function<void(inotify_event *)>, uint32_t mask = IN_MODIFY | IN_DELETE);
private:
    std::thread m_main_loop_thread;
    std::atomic<int> m_inotify_handle_fd;
    std::unordered_map<int, std::pair<std::string, std::function<void(inotify_event *)>>> m_watch_handlers;
};
