#include "repository_paths.h"
#include <global.h>
repository_paths::repository_paths(const std::filesystem::path &root)
    :m_root_path(root)
{

}

std::filesystem::path repository_paths::repo() const
{
    return m_root_path / global::repo_path;
}

std::filesystem::path repository_paths::terminal(const branch_t &branch, const repo_t &repository) const
{
    return m_root_path / branch / repository;
}

std::filesystem::path repository_paths::repo_package(const std::string &package_name) const
{
    return m_root_path / global::repo_path / package_name;
}

std::filesystem::path repository_paths::package(const branch_t &branch, const repo_t &repository, const std::string &package_name) const
{
    return terminal(branch, repository) / package_name;
}

std::filesystem::path repository_paths::database(const branch_t &branch, const repo_t &repository) const
{
    return terminal(branch,repository) / (repository + ".db");
}

std::filesystem::path repository_paths::signature(const branch_t &branch, const repo_t &repository, const std::string &package_name) const
{
    return terminal(branch, repository) / (package_name+".sig");
}

std::filesystem::path repository_paths::overlay() const
{
    return m_root_path / global::overlay_path;
}

std::filesystem::path repository_paths::overlay_package(const std::string &package_name) const
{
    return m_root_path / global::overlay_path / package_name;
}

std::filesystem::__cxx11::path repository_paths::root() const{
    return m_root_path;
}
