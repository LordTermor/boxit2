#pragma once
#include <filesystem>
#include <string>
#include <types.h>
class repository_paths
{
public:
    repository_paths(const std::filesystem::path& root);

    std::filesystem::path overlay() const;

    std::filesystem::path terminal(const branch_t& branch, const repo_t& repository) const;

    std::filesystem::path repo_package(const std::string& package_name) const;

    std::filesystem::path package(const branch_t& branch, const repo_t& repository, const std::string& package_name) const;

    std::filesystem::path database(const branch_t& branch, const repo_t& repository) const;

    std::filesystem::path signature(const branch_t &branch, const repo_t &repository, const std::string &package_name) const;

    std::filesystem::path repo() const;

    std::filesystem::path overlay_package(const std::string& package_name) const;
    
    std::filesystem::path root() const;


private:
    std::filesystem::path m_root_path;
};
