#include "access_queue.h"
#include <filesystem>
#include <boost/process.hpp>
#include <boost/log/trivial.hpp>
#include <fmt/format.h>

bool access_queue::enqueue(const std::shared_ptr<access_command_base>& request) {
        std::lock_guard guard(m_queue_mutex);
        m_queue.push(request);

        if(!m_is_processing){
            process();
        }

        return true;
    }

void access_queue::process() {
    m_is_processing = true;
    std::thread processor ([this](){
        while(!m_queue.empty()){
            auto request = m_queue.front();
            m_queue.pop();
            (*request)();
        }
        m_is_processing = false;
    });
    processor.detach();
}
