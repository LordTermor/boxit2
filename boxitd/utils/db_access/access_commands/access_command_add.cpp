#include "access_command_add.h"
#include <boost/log/trivial.hpp>
#include <fmt/format.h>
bool access_command_add::repo_add()
{

    std::vector<std::string> args(m_package_list.size()+1);

    args[0] = m_paths->database(m_branch, m_repo).string()+".tar.xz";

    std::transform(m_package_list.begin(),
                   m_package_list.end(),
                   args.begin()+1,
                   [this](const package& pkg) {
                       return m_paths->package(m_branch,m_repo,pkg.filename());
                });

    return repo_script("repo-add", args);
}

bool access_command_add::create_symlink(const package& package)
{

    BOOST_LOG_TRIVIAL(info) << fmt::format("Creating symlink of {2} in {0}/{1}", m_branch, m_repo, package.filename());


    std::filesystem::path relative_overlay;
    if(m_storage == REPO || m_storage == AUTO){
        relative_overlay = std::filesystem::relative(m_paths->repo(), m_paths->terminal(m_branch,m_repo));
    } else {
        relative_overlay = std::filesystem::relative(m_paths->overlay(), m_paths->terminal(m_branch,m_repo));
    }

    if(!std::filesystem::exists(m_paths->package(m_branch,m_repo,package.filename()))){
        std::filesystem::create_symlink(relative_overlay / package.filename(),
                                        m_paths->package(m_branch, m_repo, package.filename()));
    }
    if(!std::filesystem::exists(m_paths->signature(m_branch,m_repo,package.filename()))){
        std::filesystem::create_symlink(relative_overlay / (package.filename()+".sig"),
                                        m_paths->signature(m_branch, m_repo, package.filename()));
    }

    return true;
}
