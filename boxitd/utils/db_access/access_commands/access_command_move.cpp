#include "access_command_move.h"
#include <filesystem>
#include <boost/log/trivial.hpp>
#include <fmt/format.h>

void access_command_move::operator ()()
{
    BOOST_LOG_TRIVIAL(info) << fmt::format("Moving {0}/{2} to {1}/{2}", m_branch, m_target_branch, m_repo);

    namespace fs = std::filesystem;
    for(const auto& file
        : fs::directory_iterator(m_paths->terminal(m_target_branch, m_repo))){
        fs::remove(file);
    }


    fs::copy(m_paths->terminal(m_branch, m_repo),
             m_paths->terminal(m_target_branch, m_repo),
             fs::copy_options::recursive|fs::copy_options::copy_symlinks);

    m_callback(status::SUCCESS);

}
