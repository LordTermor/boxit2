#pragma once
#include "access_command_base.h"

class access_command_remove : public access_command_base {
public:
    template<typename TContainer>
    access_command_remove(const access_command_base::access_options<TContainer>&opts):access_command_base(opts){}

    void operator()() override {
        for(const auto& pkg : m_package_list){
            remove_symlink(pkg);
        }
        repo_remove();
        m_callback(status::SUCCESS);
    };
private:
    bool repo_remove();
    bool remove_symlink(const package& pkg);

};
