#pragma once
#include "access_command_base.h"

class access_command_replace : public access_command_base {
public:
    template<typename TContainer>
    access_command_replace(const access_command_base::access_options<TContainer>&opts):access_command_base(opts){}
};
