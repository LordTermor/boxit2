#include "access_command_remove.h"
#include <boost/log/trivial.hpp>
#include <fmt/format.h>

bool access_command_remove::repo_remove()
{
//     BOOST_LOG_TRIVIAL(info) << fmt::format("Removing {2} from {0}/{1}", branch, repo, pkg.name());

    std::vector<std::string> args(m_package_list.size()+1);

    args[0] = m_paths->database(m_branch, m_repo).string()+".tar.xz";

    std::transform(m_package_list.begin(),
                   m_package_list.end(),
                   args.begin()+1,
                   [](const package& pkg) {
                       return pkg.name();
                });

    return repo_script("repo-remove", args);
}

bool access_command_remove::remove_symlink(const package& pkg)
{

    BOOST_LOG_TRIVIAL(info) << fmt::format("Removing symlink of {2} from {0}/{1} along with the package", m_branch, m_repo, pkg.filename());


    std::filesystem::remove(m_paths->package(m_branch, m_repo, pkg.filename()));
    std::filesystem::remove(m_paths->signature(m_branch, m_repo, pkg.filename()));

    if(m_storage == REPO || m_storage == AUTO){
        std::filesystem::remove(m_paths->repo_package(pkg.filename()));
        std::filesystem::remove(m_paths->repo() / (pkg.filename()+".sig"));
    }

    if(m_storage == OVERLAY || m_storage == AUTO){
        std::filesystem::remove(m_paths->overlay_package(pkg.filename()));
        std::filesystem::remove(m_paths->overlay() / (pkg.filename()+".sig"));
    }



    return true;
}
