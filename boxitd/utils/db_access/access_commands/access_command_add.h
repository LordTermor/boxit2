#pragma once
#include "access_command_base.h"

class access_command_add : public access_command_base {
public:
    template<typename TContainer>
    access_command_add(const access_command_base::access_options<TContainer>&opts):access_command_base(opts){}

    virtual void operator()() override {
        for(const auto& pkg : m_package_list){
            create_symlink(pkg);
        }
        repo_add();
        m_callback(status::SUCCESS);
    }

private:
    bool repo_add();
    bool create_symlink(const package& pkg);
};
