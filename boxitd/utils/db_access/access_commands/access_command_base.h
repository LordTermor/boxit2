#pragma once
#include <types.h>
#include <functional>
#include <package.h>
#include <string>
#include <utils/repository_paths.h>
#include <utils.h>

class access_command_base {
public:



    enum package_storage {
        REPO, OVERLAY,AUTO
    };

    enum status {
        SUCCESS,FAILED
    };

    template<typename TContainer>
    struct access_options {
        package_storage storage;
        branch_t branch;
        repo_t repo;
        const TContainer& package_list;
        std::function<void(status st)> callback;
        repository_paths& paths;
    };

    template <typename TContainer>
    access_command_base(const access_options<TContainer>& opts):
                   m_storage(opts.storage),
                   m_branch(opts.branch),
                   m_repo(opts.repo),
                   m_package_list(opts.package_list.begin(),opts.package_list.end()),
                   m_callback(opts.callback),
                   m_paths(&opts.paths)
    {

    }
    virtual ~access_command_base() =default;

    virtual void operator()() = 0;

protected:
    template<typename TContainer>
    bool repo_script(const std::string& script_path, const TContainer& arguments){
        namespace bp = boost::process;

        auto script = utils::get_script(script_path);

        if(!script.has_value()){return false;}

        auto child = bp::child(script.value().string(), bp::args(arguments));
        child.wait();

        return child.exit_code() == 0;
    }

    package_storage m_storage;
    branch_t m_branch;
    repo_t m_repo;
    std::vector<package> m_package_list;
    std::function<void(status st)> m_callback;
    repository_paths* m_paths;
};
