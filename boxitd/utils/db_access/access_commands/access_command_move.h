#pragma once
#include "access_command_base.h"
class access_command_move : public access_command_base
{
public:
    template<typename TContainer>
    access_command_move(const access_command_base::access_options<TContainer>&opts, const branch_t& target): access_command_base(opts), m_target_branch(target){}

    // access_command_base interface
public:
    virtual void operator ()() override;

private:
    branch_t m_target_branch;
};
