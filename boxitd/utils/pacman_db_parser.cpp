#include "pacman_db_parser.h"


pacman_db_parser::pacman_db_parser(const std::string &inmemory_desc)
    :m_contents(inmemory_desc)
{

}

std::string pacman_db_parser::operator[](const std::string &key) const
{
    return get(key);
}

std::string pacman_db_parser::get(const std::string &key) const
{
    auto prepared_key = "%"+key+"%";

    auto value_begin = m_contents.find(prepared_key+"\n")+prepared_key.size()+1;

    auto value_end = m_contents.find("\n", value_begin);

    return m_contents.substr(value_begin,value_end-value_begin);
}
