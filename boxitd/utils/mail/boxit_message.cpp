#include "boxit_message.h"
#include <fmt/format.h>
#include <mailio/quoted_printable.hpp>
#include <global.h>
auto constexpr content_header_template = "### BoxIt2 Memo ### \r\n"
                                         "User {} commited following changes: \r\n"
                                         "- {} {} {}:  {} new and {} removed package(s)";
boxit_message::boxit_message()
{

}

void boxit_message::repository(const repo_t &repo)
{
    m_repo = repo;
}

void boxit_message::branch(const branch_t &branch)
{
    m_branch = branch;
}

void boxit_message::user(const std::string &user)
{
    m_user = user;
}

void boxit_message::arch(package_arch arch)
{
    m_arch = arch;
}

mailio::message boxit_message::construct() const
{
    using namespace mailio;
    message msg;
    msg.from(mail_address("BoxIt2", "boxit@manjaro.org"));
    msg.add_recipient(mail_address("Manjaro Packages", global::settings.get<std::string>("mail-address").value_or("agrinev98@gmail.com"))); ///TODO change default email address
    msg.subject(fmt::format("[BoxIt2] Memo {}", m_arch==package_arch::x86_64?"x86_64":"arm"));
    auto content = fmt::format(content_header_template,
                               m_user, m_repo, m_branch, m_arch==package_arch::x86_64?"x86_64":"arm", m_new_packages.size(), m_removed_packages.size());

    msg.content_transfer_encoding(mime::content_transfer_encoding_t::QUOTED_PRINTABLE);

    msg.content(content);

    std::stringstream added,
            removed;


    added << "[New Packages]\n";
    for(const auto& new_pkg : m_new_packages){
        added << new_pkg.filename() << "\n";
    }
    removed << "[Removed Packages]\n";
    for(const auto& removed_pkg : m_removed_packages){
        removed << removed_pkg.filename() << "\n";
    }

    msg.attach({
                   {added, "added", {mime::media_type_t::TEXT, "plain"}},
                   {removed, "removed", {message::media_type_t::TEXT,"plain"}}
               });


    return msg;
}
