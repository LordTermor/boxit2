#pragma once
#include <mailio/message.hpp>
#include <package.h>
#include <vector>
#include <types.h>
class boxit_message
{
public:
    boxit_message();

    void repository(const repo_t& repo);
    void branch(const branch_t& branch);
    void user(const std::string& user);
    void arch(package_arch arch);

    template<class TIterable>
    void new_packages(const TIterable& pkgs)
    {
        std::copy(pkgs.begin(), pkgs.end(), std::inserter(m_new_packages, m_new_packages.begin()));
    }
    template<class TIterable>
    void removed_packages(const TIterable& pkgs)
    {
        std::copy(pkgs.begin(), pkgs.end(), std::inserter(m_removed_packages, m_removed_packages.begin()));
    }

    operator mailio::message() const{
        return construct();
    }
private:
    mailio::message construct() const;
    repo_t m_repo;
    branch_t m_branch;
    std::string m_user;
    package_arch m_arch;
    std::vector<package> m_new_packages;
    std::vector<package> m_removed_packages;
};
