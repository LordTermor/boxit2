#include "user_controller.h"
#include <services/user_service.h>
#include <boost/log/trivial.hpp>
#include <services/permissions_service.h>
#include <utils.h>

grpc::Status user_controller::AddUser(grpc::ServerContext *context, const AddUserRequest *request, [[maybe_unused]] Empty *response)
{
    
    if(!permissions_service::check(utils::extract_metadata(context, "username"), permission({"users", "add"}))){
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "User doesn't have a permission to add user");
    }
    
    
    return user_service::add_user(request->name(),request->password())
    ? grpc::Status::OK
    : grpc::Status(grpc::StatusCode::INTERNAL, "Cannot add user");
}

grpc::Status user_controller::RemoveUser(grpc::ServerContext *context, const RemoveUserRequest *request, [[maybe_unused]] Empty *response)
{
     if(!permissions_service::check(utils::extract_metadata(context, "username"), permission({"users", "remove"}))){
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "User doesn't have a permission to remove user");
    }
    auto status = user_service::remove_user(request->name());
    
    BOOST_LOG_TRIVIAL(info)<<"Print\n" << status;
    
    return status? grpc::Status(grpc::StatusCode::OK,"")
    : grpc::Status(grpc::StatusCode::INTERNAL, "Cannot remove user");;
}
