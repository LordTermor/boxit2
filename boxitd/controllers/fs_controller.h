#pragma once
#include <controllers/base_controller.h>
#include <protos/filesystem.pb.h>
#include <protos/filesystem.grpc.pb.h>
class fs_controller : public FileSystem::Service
{
public:
    ::grpc::Status GetRoot(::grpc::ServerContext * context, const ::RootRequest * request, ::RootReply * response) override;
    ::grpc::Status GetBranch(::grpc::ServerContext * context, const ::BranchRequest * request, ::BranchReply * response) override;
    ::grpc::Status GetRepoPackages(::grpc::ServerContext * context, const ::PackagesRequest * request, ::PackagesReply * response) override;
    ::grpc::Status GetPackage(::grpc::ServerContext * context, const ::PackageRequest * request, ::PackageReply * response) override;
    ::grpc::Status WriteOverlay(::grpc::ServerContext *context, ::grpc::ServerReader<WriteRequest> *reader, ::WriteReply *response) override;
    ::grpc::Status RegisterPackage(::grpc::ServerContext * context, const RegisterRequest * request, ::RegisterReply * response) override;
    ::grpc::Status RemovePackage(::grpc::ServerContext *context, const ::RemoveRequest *request, ::RemoveReply *response) override;
    ::grpc::Status Exists(::grpc::ServerContext * context, const ::ExistsRequest * request, ::ExistsReply * response) override;
};
