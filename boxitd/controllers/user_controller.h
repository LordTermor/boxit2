#pragma once
#include <protos/user.pb.h>
#include <protos/user.grpc.pb.h>
#include <controllers/base_controller.h>

class user_controller: public UserController::Service
{
public:
    virtual grpc::Status AddUser(grpc::ServerContext *context, const AddUserRequest *request, Empty *response) override;
    virtual grpc::Status RemoveUser(grpc::ServerContext *context, const RemoveUserRequest *request, Empty *response) override;
};
