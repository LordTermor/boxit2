#include "auth_controller.h"
auth_controller::auth_controller()
{

}

AuthReply auth_controller::authorize(const AuthRequest &rq)
{
    AuthReply reply;

    reply.set_ok(auth_service::authorize(rq.name(), rq.password()));

    return reply;
}


