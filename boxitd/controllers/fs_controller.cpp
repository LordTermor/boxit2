#include "fs_controller.h"
#include <global.h>
#include <services/fs_service.h>
#include <utils/grpc_streambuf.h>
#include <fstream>
#include <boost/log/trivial.hpp>
#include <services/permissions_service.h>
grpc::Status fs_controller::GetRoot(grpc::ServerContext* context,[[maybe_unused]] const RootRequest* request, RootReply* response)
{
    auto branches = fs_service::get_branches();
    
     for(const auto&branch : branches){
        if(permissions_service::check(utils::extract_metadata(context, "username"), permission({"fs", "read", branch, "*", "*"}))){
            response->add_branches(branch);
        }
    }

    return grpc::Status::OK;
}

grpc::Status fs_controller::GetBranch(grpc::ServerContext *context, const BranchRequest *request, BranchReply *response)
{
    auto repos = fs_service::get_repos(request->name());

    for(const auto&repo : repos){
        if(permissions_service::check(utils::extract_metadata(context, "username"), permission({"fs", "read", request->name(), repo, "*"}))){
            response->add_repos(repo);
        }
    }

    return grpc::Status::OK;
}

grpc::Status fs_controller::GetRepoPackages(grpc::ServerContext *context, const PackagesRequest *request, PackagesReply *response)
{
    if(!permissions_service::check(utils::extract_metadata(context, "username"), permission({"fs", "read", request->branch(), request->repo(), "*"}))){
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "User doesn't have a permission to read this");
    }
    auto pkgs = fs_service::get_packages(request->branch(), request->repo());

    
    for(const auto& pkg : pkgs){
        response->add_packages(pkg);
    }


    return grpc::Status::OK;
}

grpc::Status fs_controller::GetPackage(grpc::ServerContext *context, const PackageRequest *request, PackageReply *response)
{

}

grpc::Status fs_controller::WriteOverlay(grpc::ServerContext *context, ::grpc::ServerReader<WriteRequest> *reader, WriteReply *response)
{
   
    WriteRequest rq;

    reader->Read(&rq);

    if(rq.content_case()!=WriteRequest::ContentCase::kMetadata){
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "Metadata should go first");
    }
    if(!permissions_service::check(utils::extract_metadata(context, "username"), permission({"fs", "write", "*", "*", rq.metadata().name()}))){
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "User doesn't have a permission to write this");
    }

    fs_service::write(rq.metadata().name(), rq.metadata().offset(),reader);

    return grpc::Status::OK;

}

grpc::Status fs_controller::RegisterPackage(grpc::ServerContext* context, const RegisterRequest* request, RegisterReply* response)
{
    if(!permissions_service::check(utils::extract_metadata(context, "username"), permission({"fs", "write", request->branch(), request->repo(), request->package_name()}))){
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "User doesn't have a permission to write this");
    }
    
    fs_service::register_package(request->branch(), request->repo(), request->package_name());
    
    return grpc::Status::OK;
}



grpc::Status fs_controller::RemovePackage(grpc::ServerContext *context, const RemoveRequest *request, RemoveReply *response)
{
    if(!permissions_service::check(utils::extract_metadata(context, "username"), permission({"fs", "remove", request->branch(), request->repo(), request->package_name()}))){
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "User doesn't have a permission to remove this");
    }
    return fs_service::remove(request->branch(), request->repo(), request->package_name())
           ?grpc::Status::OK
           :grpc::Status(grpc::StatusCode::NOT_FOUND, "File cannot be removed");
}

grpc::Status fs_controller::Exists(grpc::ServerContext* context, const ExistsRequest* request, ExistsReply* response)
{
    auto perm = permission{
        "fs",
        "read",
        request->branch().empty()?"*":request->branch(),
        request->repo().empty()?"*":request->repo(),
        request->package_name().empty()?"*":request->package_name()
    };
    
    if(!permissions_service::check(utils::extract_metadata(context, "username"), perm)){
        return grpc::Status(grpc::StatusCode::UNAUTHENTICATED, "User doesn't have a permission to read this");
    }
    
    return fs_service::exists(request->branch(), request->repo(), request->package_name(), request->is_signature())
           ?grpc::Status::OK
           :grpc::Status(grpc::StatusCode::NOT_FOUND, "File doesn't exist");
    
}

