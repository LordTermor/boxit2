#pragma once
#include <functional>
#include <map>
#include "auth_controller.h"
#include "user_controller.h"
#include "package_controller.h"
#include "fs_controller.h"
#include "log_controller.h"
#include "routes.h"

class router {
private:
    template<class TRequest, class TReply>
      static std::function<void (std::istream&, std::ostream&)> wrap( TReply func(const TRequest&)){

          return [=](std::istream& istream, std::ostream& ostream){
              TRequest req;
              req.ParseFromIstream(&istream);

              TReply reply = func(req);
              reply.SerializeToOstream(&ostream);
          };
      }
    template<class T, class TRequest, class TReply>
    static std::function<void (std::istream&, std::ostream&)> wrap(TReply (T::*fun)(const TRequest&)){

        return [=](std::istream& istream, std::ostream& ostream){
            AuthRequest authrq;
            TReply reply;

            authrq.ParseFromIstream(&istream);

            if(!auth_service::authorize(authrq.name(),authrq.password())){
                reply.SerializeToOstream(&ostream);
                return;
            }
            auto controller_instance = T();
            controller_instance.set_user_name(authrq.name());

            TRequest req;
            req.ParseFromIstream(&istream);

            reply = (controller_instance.*fun)(req);

            reply.SerializeToOstream(&ostream);

        };
    }


public:
    inline static const std::map<int, std::function<void (std::istream&,std::ostream&)>> routes = {
//     {MessageType::Auth, wrap(&auth_controller::authorize)},
            //    {MessageType::AddUser, streamify(&user_controller::add_user)},
            //    {MessageType::RemoveUser, streamify(&user_controller::remove_user)},
//     {MessageType::Pull, wrap(&package_controller::pull)},
//     {MessageType::Sync, wrap(&package_controller::sync)},
            //    {MessageType::Snap, streamify(&package_controller::snap)},
//     {MessageType::Fs, wrap(&fs_controller::fs)}
};

};
