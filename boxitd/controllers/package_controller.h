#pragma once
#include <protos/package.pb.h>
#include <protos/package.grpc.pb.h>
#include <controllers/base_controller.h>
class package_controller: public PackageController::Service
{
    grpc::Status Snap(grpc::ServerContext * context, const ::SnapshotRequest * request, ::SnapshotReply * response) override;
    grpc::Status Sync(grpc::ServerContext * context, const ::SyncRequest * request, ::SyncReply * response) override;
    grpc::Status Compare(grpc::ServerContext *context, const CompareRequest *request, CompareReply *response) override;
};
