#pragma once
#include <string>
class base_controller
{
public:
    base_controller();

    std::string user_name() const{return m_user_name;};
    void set_user_name(const std::string &value){m_user_name=value;};


protected:
    std::string m_user_name;
};
