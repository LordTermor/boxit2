#pragma once
#include <string>
#include <protos/auth.pb.h>
#include <services/auth_service.h>
class auth_controller
{
public:
    auth_controller();

    static AuthReply authorize(const AuthRequest&);


};
