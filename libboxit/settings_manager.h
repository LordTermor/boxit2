#ifndef SETTINGS_MANAGER_H
#define SETTINGS_MANAGER_H
#include <yaml-cpp/yaml.h>
#include <filesystem>
#include <iostream>

class settings_manager
{
public:
    settings_manager(const std::filesystem::path& path);
    
    template<class T>
    std::optional<T> get(const std::string& key) const
    {
        if(m_settings_node[key].IsDefined()){
            return m_settings_node[key].as<T>();
        }
        return std::optional<T>();
    }
    
    template<class T>
    std::optional<T> get(const std::initializer_list<std::string>& names) const
    {
        auto node = m_settings_node;
        try {
            for(const auto& name : names){
                node.reset(node[name]);
            }
            return node.as<T>();
        } catch (...) {
            return {};
        }
    }
    
    template<class T>
    bool set(const std::string& key, const T& value){
        try{
            m_settings_node[key] = value;
            return true;
        }catch(...){
            return false;
        }
    }
    
    template<class T>
    bool set(const std::initializer_list<std::string>& names, const T& value){
        YAML::Node node = m_settings_node;
        try {
            auto it = names.begin();
            
            while( it != names.end() - 1 ){
                if( !node[*it].IsDefined() ){
                    node[*it] = YAML::Node();
                }
                
                node.reset(node[*it]);
                ++it;
            }
            
            node[*it] = value;
            return true;
        } catch(...){
            return false;
        }
    }
    
    void reload();
    void save() const;
    
    
    
private:
    
    
    std::filesystem::path m_path;
    YAML::Node m_settings_node;
};

#endif // SETTINGS_MANAGER_H
