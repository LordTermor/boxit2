#pragma once
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <sstream>
#include <string>
#include <iomanip>
#include <iostream>
#include <grpc++/grpc++.h>
#include <boost/process.hpp>
#include <filesystem>
#include <bitset>
namespace utils {
    
    inline std::string extract_metadata(::grpc::ServerContext* ctx,const std::string& key)
    {
        auto it = ctx->auth_context()->FindPropertyValues(key)[0];
        return {it.begin(), it.end()};
    }
    inline std::vector<uint8_t> sha256_raw(const std::string& str){
        unsigned char hash[SHA256_DIGEST_LENGTH];
        SHA256_CTX sha256;
        SHA256_Init(&sha256);
        SHA256_Update(&sha256, str.c_str(), str.size());
        SHA256_Final(hash, &sha256);
        return {hash, hash+SHA256_DIGEST_LENGTH};
    }
    inline std::string sha256(const std::string& str)
    {
        auto hash = sha256_raw(str);
        std::stringstream ss;
        for(const auto& element : hash)
        {
            ss << std::hex << std::setw(2) << std::setfill('0') << (int)element;
        }
        return ss.str();
    }
    
    std::vector<uint8_t> encrypt(const std::string& plain_text,
                                 const std::vector<uint8_t>& iv = {});
    std::string decrypt(const std::vector<uint8_t>& cipher_text,
                        const std::vector<uint8_t>& iv = {});
    inline std::string box =
    "                                               \n"
    " ______                    _____  _     _____  \n"
    "|_   _ \\                  |_   _|/ |_  / ___ `.\n"
    "  | |_) |   .--.   _   __   | | `| |-'|_/___) |\n"
    "  |  __'. / .'`\\ \\[ \\ [  ]  | |  | |   .'____.'\n"
    " _| |__) || \\__. | > '  <  _| |_ | |, / /_____ \n"
    "|_______/  '.__.' [__]`\\_]|_____|\\__/ |_______|\n"
    "                                               \n"
    "\n";
    
    inline void draw_logo() {
        std::cout<<box;
    }
    constexpr timespec timepointToTimespec(
        std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> tp)
    {
        auto secs = std::chrono::time_point_cast<std::chrono::seconds>(tp);
        auto ns = std::chrono::time_point_cast<std::chrono::nanoseconds>(tp) -
        std::chrono::time_point_cast<std::chrono::nanoseconds>(secs);

        return timespec{secs.time_since_epoch().count(), ns.count()};
    }


    inline std::optional<std::filesystem::path> get_script(const std::string& script)
    {
        namespace fs = std::filesystem;
        namespace bp = boost::process;

        //cache found scripts
        static std::unordered_map<std::string, std::optional<fs::path>> found_scripts;

        auto cached_path = found_scripts.find(script);
        if(cached_path != found_scripts.end()){
            return cached_path->second;
        }

        std::optional<fs::path> full_path = bp::search_path(script).string();

        if(full_path->empty()){
            full_path = fs::absolute(fs::path("scripts") / script);
            if(!fs::exists(*full_path)){
                full_path.reset();
            }
        }
        if(full_path &&
                !bool(fs::status(*full_path).permissions() & fs::perms::owner_exec)) {
            full_path.reset();
        }
        found_scripts[script] = full_path;
        return full_path;
    }


}
