#pragma once
#include <unordered_map>
#include <utility>
#include <set>

template <typename TKey1, typename TKey2, typename TValue>
class double_map : public std::unordered_map<std::pair<TKey1, TKey2>, TValue>
{
public:
    double_map() = default;

    std::set<TKey1> row_names() const{return m_row_names;};
    std::set<TKey1> column_names() const{return m_column_names;};

    void emplace(const TKey1& i,const TKey2& j, const TValue& value){
        m_row_names.insert(i);
        m_column_names.insert(j);
        this->insert({{i,j},value});
    }

private:
    class double_map_index_proxy{
        double_map* parent;
        TKey1 i;
    public:
        double_map_index_proxy(double_map* map,const TKey1& i):parent(map),i(i){}
        TValue& operator[](const TKey2& j) const{
            return parent->at(std::make_pair(i,j));
        }
    };
    std::set<TKey1> m_row_names;
    std::set<TKey2> m_column_names;

public:
    double_map_index_proxy operator[](const TKey1& i){
        return double_map_index_proxy(this, i);
    }


};
