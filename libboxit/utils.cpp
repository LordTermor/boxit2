#include "utils.h"


namespace utils {
    static auto crypto_key_hash = sha256_raw(CRYPTO_KEY);
    std::vector< uint8_t > encrypt(const std::string& plain_text, const std::vector< uint8_t >& iv)
    {
        auto ctx = std::unique_ptr<EVP_CIPHER_CTX,
        decltype(&EVP_CIPHER_CTX_free)>
        (EVP_CIPHER_CTX_new(), EVP_CIPHER_CTX_free);
        
        
        int total_len, len;
        
        std::vector<uint8_t> cipher_text;
        cipher_text.resize(1024);
        
        
        if(1 != EVP_EncryptInit_ex(ctx.get(), EVP_aes_256_cbc(), NULL, crypto_key_hash.data(), iv.data()))
            throw std::exception();
        
        
        if(1 != EVP_EncryptUpdate(ctx.get(), cipher_text.data(), &len, reinterpret_cast<const uint8_t*>(plain_text.data()), plain_text.length()))
            throw std::exception();
        total_len = len;
        
        if(1 != EVP_EncryptFinal_ex(ctx.get(), cipher_text.data() + len, &len))
            throw std::exception();
        total_len+=len;
        cipher_text.erase(cipher_text.begin()+total_len, cipher_text.end());
        
        
        return cipher_text;
    }
    
    std::string decrypt(const std::vector< uint8_t >& cipher_text, const std::vector< uint8_t >& iv)
    {
        auto ctx = std::unique_ptr<EVP_CIPHER_CTX,
        decltype(&EVP_CIPHER_CTX_free)>
        (EVP_CIPHER_CTX_new(), EVP_CIPHER_CTX_free);
        
        int total_len, len;
        
        std::string plain_text;
        plain_text.resize(1024);
        
        if(1 != EVP_DecryptInit_ex(ctx.get(), EVP_aes_256_cbc(), NULL, crypto_key_hash.data(), iv.data()))
            throw std::exception();
        
        if(1 != EVP_DecryptUpdate(ctx.get(), reinterpret_cast<uint8_t*>(plain_text.data()), &len, cipher_text.data(), cipher_text.size()))
            throw std::exception();
        total_len = len;
        
        
        if(1 != EVP_DecryptFinal_ex(ctx.get(), reinterpret_cast<uint8_t*>(plain_text.data()) + len, &len)) {
            throw std::exception();
        }
        total_len+=len;
        plain_text.erase(plain_text.begin()+total_len, plain_text.end());
        
        
        return plain_text;
    }
}
