#pragma once
#include <string>

struct evr{
    static evr parse(const std::string& evr_string);

    std::string epoch;
    std::string version;
    std::string release;

    std::string string() const;
    bool empty() const {return epoch.empty() && version.empty() && release.empty();}

    static int compare(const evr& lh, const evr& rh);

    bool operator<(const evr& other)const{
        return compare(*this, other)==-1;
    }

    bool operator>(const evr& other)const{
        return compare(*this, other)==1;
    }

    bool operator==(const evr& other)const{
        return compare(*this, other)==0;
    }

    bool operator<=(const evr& other)const{
        auto ret = compare(*this, other);

        return ret==-1 || ret==0;
    }
    bool operator>=(const evr& other)const {
        auto ret = compare(*this, other);

        return ret==1 || ret==0;
    }
};

class package
{

public:
    evr version() const;
    std::string filename() const {return m_file_name;}
    std::string name() const;

    package(const std::string& fname):m_file_name(fname){}

    bool operator < (const package& other) const {
        return this->filename()<other.filename();

    }


private:
    std::string m_file_name;
};
