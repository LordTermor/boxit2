#pragma once


enum MessageType{
    Version = 0, // should always be 0
    Push,
    Pull,
    Auth,
    AddUser,
    RemoveUser,
    Sync,
    PullBranches,
    Snap,
    Fs
};
