#include "package.h"
#include <algorithm>
#include <cstring>
#include <fmt/format.h>

evr evr::parse(const std::string& evr_string)
{
    evr result;

    auto s = evr_string.find(':');


    if(s != std::string::npos) {
        result.epoch = evr_string.substr(0, s-1);

        if(result.epoch.empty()) {
            result.epoch = "0";
        }
    } else {
        result.epoch = "0";
        s = -1;
    }
    auto se = evr_string.find('-',s+1);

    result.version = evr_string.substr(s+1,se-s-1);

    result.release = evr_string.substr(se+1);
    return result;
}

std::string evr::string() const
{
    if(empty()){
        return "-";
    }
    if(epoch == "0"){
        return fmt::format("{}-{}", version, release);
    }
    return fmt::format("{}:{}-{}", epoch, version, release);
}


evr package::version() const{
    auto last_dash = m_file_name.find_last_of("-");
    auto pre_last_dash = m_file_name.find_last_of("-", last_dash-1);
    auto pre_pre_last_dash = m_file_name.find_last_of("-", pre_last_dash-1);

    return evr::parse(m_file_name.substr(pre_pre_last_dash+1, last_dash-pre_pre_last_dash-1));
}

std::string package::name() const
{
    auto last_dash = m_file_name.find_last_of("-");
    auto pre_last_dash = m_file_name.find_last_of("-", last_dash-1);
    auto pre_pre_last_dash = m_file_name.find_last_of("-", pre_last_dash-1);

    return m_file_name.substr(0, pre_pre_last_dash);
}

//naive rewrite of original rpmvercmp from alpm
int rpmvercmp(const std::string& a_orig,const std::string& b_orig)
{
    if(a_orig==b_orig) return 0;

    std::string a = a_orig,
            b = b_orig;

    auto one = a.begin(),
            it1 = a.begin();
    auto two = b.begin(),
            it2 = b.begin();

    bool isnum;

    while(one != a.end() && two != b.end()){

        while(one!=a.end() && !std::isalnum(*one)) one++;
        while(two!=b.end() && !std::isalnum(*two)) two++;

        if(one==a.end() || two==b.end()) break;

        if ((one-it1)!=(two-it2)){
            return (one - it1) < (two - it2) ? -1 : 1;;
        }

        it1 = one;
        it2 = two;

        if(std::isdigit(*it1)){
            while(it1!=a.end() && std::isdigit(*one)) one++;
            while(it2!=b.end() && std::isdigit(*two)) two++;
            isnum = true;
        } else {
            while(it1 != a.end() && std::isalpha(*it1)) it1++;
            while(it2 != b.end() && std::isalpha(*it2)) it2++;
            isnum = false;
        }

        char oldch1 = *it1;
        *it1 = 0;
        char oldch2 = *it2;
        *it2 = 0;

        if ( one == it1 ){
            return -1;
        }

        if ( two == it2 ){
            return isnum?1:-1;
        }


        if(isnum) {

            while(*one == '0') one++;
            while(*two == '0') two++;

            if ( std::distance(one, a.end()) > std::distance(two, b.end())  ){
                return 1;
            }
            if ( std::distance(one, a.end()) < std::distance(two, b.end()) ){
                return -1;
            }

        }
        auto substr1 = std::string(one,a.end());
        auto substr2 = std::string(two,b.end());
        auto rc = substr1.compare(substr2);

        if(rc){
            return rc < 1 ? -1 : 1;
        }


        *it1 = oldch1;
        one = it1;

        *it2 = oldch2;
        two = it2;

    }

    if(one == a.end() && two == b.end()){
        return 0;
    }

    if ( (one == a.end() && std::isalpha(*two))
         || std::isalpha(*one)){
        return -1;
    } else {
        return 1;
    }


}

int evr::compare(const evr& evr1, const evr& evr2)
{
    int ret;

    ret = rpmvercmp(evr1.epoch, evr2.epoch);
    if(ret == 0) {
        ret = rpmvercmp(evr1.version, evr2.version);
        if(ret == 0 && evr1.release.length()>0 && evr2.release.length()>0) {
            ret = rpmvercmp(evr1.release, evr2.release);
        }
    }

    return ret;
}


